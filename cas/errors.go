package cas

// IsClosed 返回 錯誤是否是 設備 已關閉
func IsClosed(e error) bool {
	return e == errQueueAlreadyClosed
}

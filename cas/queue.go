package cas

import (
	"errors"
	"fmt"
	"runtime"
	"sync/atomic"
)

var errQueueAlreadyClosed = errors.New("queue already closed")

// BlockingWaitStrategy 阻塞時的 等待策略
type BlockingWaitStrategy interface {
	// 如果 e != nil 將直接以 e 返回給 阻塞的 調用者
	//
	// ok == true 將繼續 阻塞 直到有數據到來 ok == false 立刻停止阻塞以 false狀態 返回給 調用者
	Wait() (ok bool, e error)
}

type queueElement struct {
	index uint64
	value interface{}
}

// Queue 使用 cas 實現的 無鎖 線程安全隊列
type Queue struct {
	cacheLinePadding0 [8]uint64
	putIndex          uint64 // 寫入位置
	cacheLinePadding1 [8]uint64
	takeIndex         uint64 // 讀取位置
	cacheLinePadding2 [8]uint64
	mask              uint64 // 取餘 的 位運算 掩碼
	closed            uint64 // 隊列關閉狀態
	cacheLinePadding3 [8]uint64
	elements          []queueElement
}

// NewQueue 創建隊列
func NewQueue(capacity uint64) (*Queue, error) {
	q := &Queue{}
	e := q.init(capacity)
	if e != nil {
		return nil, e
	}
	return q, nil
}

// init .
func (q *Queue) init(capacity uint64) (e error) {
	if capacity < 2 || capacity%2 != 0 {
		e = fmt.Errorf("not support capacity : %v", capacity)
		return
	}
	q.elements = make([]queueElement, capacity)
	for i := uint64(0); i < capacity; i++ {
		q.elements[i].index = i
	}
	q.mask = capacity - 1
	return
}

// Push 寫入 數據
func (q *Queue) Push(value interface{}) (bool, error) {
	return q.PushWithStrategy(value, nil)
}

// PushWithStrategy 使用指定策略 寫入數據
func (q *Queue) PushWithStrategy(value interface{}, strategy BlockingWaitStrategy) (bool, error) {
	var element *queueElement
	var pos uint64
	for {
		if atomic.LoadUint64(&q.closed) == 1 {
			return false, errQueueAlreadyClosed
		}

		pos = atomic.LoadUint64(&q.putIndex)
		element = &q.elements[pos&q.mask]
		index := atomic.LoadUint64(&element.index)
		diff := index - pos
		if diff == 0 {
			if atomic.CompareAndSwapUint64(&q.putIndex, pos, pos+1) {
				// 申請寫入 成功
				break
			}
		} else if diff < 0 {
			panic(`ring buffer in a compromised state during a push operation.`)
		}
		if strategy == nil {
			// 讓出 cpu 讓其它 goroutine 執行
			runtime.Gosched()
		} else {
			ok, e := strategy.Wait()
			if e != nil {
				return false, e
			} else if !ok {
				return false, nil
			}
		}
	}
	// 寫入數據
	element.value = value
	atomic.StoreUint64(&element.index, pos+1)
	return true, nil
}

// Pop 取出 數據
func (q *Queue) Pop() (interface{}, bool, error) {
	return q.PopWithStrategy(nil)
}

// PopWithStrategy 使用指定策略 取出數據
func (q *Queue) PopWithStrategy(strategy BlockingWaitStrategy) (interface{}, bool, error) {
	var element *queueElement
	var pos uint64
	for {
		if atomic.LoadUint64(&q.closed) == 1 {
			return nil, false, errQueueAlreadyClosed
		}

		pos = atomic.LoadUint64(&q.takeIndex)
		element = &q.elements[pos&q.mask]
		index := atomic.LoadUint64(&element.index)
		diff := index - (pos + 1)
		if diff == 0 {
			if atomic.CompareAndSwapUint64(&q.takeIndex, pos, pos+1) {
				// 申請讀取 成功
				break
			}
		} else if diff < 0 {
			panic(`ring buffer in a compromised state during a pop operation.`)
		}

		if strategy == nil {
			// 讓出 cpu 讓其它 goroutine 執行
			runtime.Gosched()
		} else {
			ok, e := strategy.Wait()
			if e != nil {
				return nil, false, e
			} else if !ok {
				return nil, false, nil
			}
		}
	}
	value := element.value
	element.value = nil
	atomic.StoreUint64(&element.index, pos+q.mask+1)
	return value, true, nil
}

// Len 返回隊列當前大小
func (q *Queue) Len() uint64 {
	return atomic.LoadUint64(&q.putIndex) - atomic.LoadUint64(&q.takeIndex)
}

// Capacity 返回隊列容量
func (q *Queue) Capacity() uint64 {
	return uint64(len(q.elements))
}

// Close 關閉隊列
func (q *Queue) Close() error {
	if atomic.CompareAndSwapUint64(&q.closed, 0, 1) {
		return nil
	}
	return errQueueAlreadyClosed
}

// IsClosed 返回隊列是否已經 關閉
func (q *Queue) IsClosed() bool {
	return atomic.LoadUint64(&q.closed) == 1
}

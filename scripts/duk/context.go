package duk

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"gopkg.in/olebedev/go-duktape.v3"
)

// GlobalModuleName 全局模塊名
const GlobalModuleName = "__king_duktape_node_modules"

// Context 包裝的 duktape 運行環境
type Context struct {
	*duktape.Context
}

// NodeInit 初始化 node 模塊
//
// root 系統模塊 查找 位置
func (ctx *Context) NodeInit(root []string) (e error) {
	ok := ctx.GetGlobalString(GlobalModuleName)
	ctx.Pop()
	if ok {
		return
	}
	ctx.PevalString(`(function(){
return {
	// 模塊 緩存
	_modules:{},
	// 二進制 模塊 初始化 函數
	_natives:{},
	// 模塊路徑 對應 全路徑
	_names:{},
	loadModule(from,name){
		var id;
		if(name[0]=="."){
			id = this.nativeID(from,name);
			if(this._modules.hasOwnProperty(id)){
				return this._modules[id];
			}
			var rs = this.nativeLoad(id);
			if(rs.hasOwnProperty("error")){
				throw rs["error"];
			}
			if(rs.hasOwnProperty("source")){
				var m = this.EvalFile(rs["dirname"],rs["filename"],rs["source"]);
				this._modules[id] = m;
				return m;
			}
		}else{
			id = this.nativeClean(name);
			if(this._modules.hasOwnProperty(id)){
				return this._modules[id];
			}
			if(this._natives.hasOwnProperty(id)){
				var rs = this._natives[id]();
				if(rs.hasOwnProperty("error")){
					throw rs["error"];
				}
				var m = rs["exports"];
				this._modules[id] = m;
				return m;
			}
			name = id;
			if(this._names.hasOwnProperty(name)){
				id = this._names[name];
				var rs = this.nativeLoad(id);
				if(rs.hasOwnProperty("error")){
					delete this._names[name];
					throw rs["error"];
				}
				if(rs.hasOwnProperty("source")){
					var m = this.EvalFile(rs["dirname"],rs["filename"],rs["source"]);
					this._modules[id] = m;
					return m;
				}else{
					delete this._names[name];
				}
			}
			var rs = this.nativeLoadABS(name);
			if(rs.hasOwnProperty("error")){
				throw rs["error"];
			}
			if(rs.hasOwnProperty("source")){
				id = rs["id"];
				var m = this.EvalFile(rs["dirname"],rs["filename"],rs["source"]);
				this._modules[id] = m;
				this._names[name] = id;
				return m;
			}
		}
		throw new Error('module not found : ' + id);
	},
	EvalFile(__dirname,__filename,__b__){
		var __ctx__ = this;
		function require(name){
			if(typeof name != "string"){
				throw new Error('require only support string'); 
			}else if(name.length < 1){
				throw new Error('module not found : ' + name); 
			}
			return __ctx__.loadModule(__filename,name);
		}
		var module = {
			exports:{},
			filename:__filename,
		};
		var exports = module.exports;
		var call = this.nativeCompile(__filename,__b__);
		if(typeof call == "function"){
			call(require,module,exports,__dirname,__filename);
		}else{
			throw call;
		}
		//eval(__b__)
		return exports;
	},
	RegisterNative(id,f){
		if(this._natives.hasOwnProperty(id)){
			throw new Error("native module already exists : " + id);
		}
		this._natives[id] = f;
	},
};
})();`,
	)
	ctx.PushGoFunction(nativeLoad)
	ctx.PutPropString(-2, "nativeLoad")
	ctx.PushGoFunction(nativeLoadABS)
	ctx.PutPropString(-2, "nativeLoadABS")
	ctx.PushGoFunction(nativeID)
	ctx.PutPropString(-2, "nativeID")
	ctx.PushGoFunction(nativeClean)
	ctx.PutPropString(-2, "nativeClean")
	ctx.PushGoFunction(nativeCompile)
	ctx.PutPropString(-2, "nativeCompile")

	n := len(root)
	if n > 0 {
		idx := ctx.PushArray()
		keys := make(map[string]bool, n)
		var ok bool
		index := uint(0)
		for i := 0; i < n; i++ {
			key := root[i]
			if filepath.IsAbs(key) {
				key = filepath.Clean(key)
			} else {
				key, e = filepath.Abs(key)
				if e != nil {
					continue
				}
			}
			if _, ok = keys[key]; ok {
				continue
			}
			keys[key] = true
			ctx.PushString(key)
			ctx.PutPropIndex(idx, index)
		}
		ctx.PutPropString(-2, "_root")
	}
	ctx.PutGlobalString(GlobalModuleName)
	return
}
func nativeLoadABS(ctx *duktape.Context) int {
	name := ctx.GetString(0)
	idx := ctx.PushObject()
	ctx.GetGlobalString(GlobalModuleName)
	ctx.GetPropString(-1, "_root")
	if ctx.IsArray(-1) {
		n := ctx.GetLength(-1)
		for i := 0; i < n; i++ {
			ctx.GetPropIndex(-1, uint(i))
			id := filepath.Clean(ctx.GetString(-1) + "/" + name)
			filename := id + ".js"
			info, e := os.Stat(filename)
			ctx.Pop()
			if e != nil {
				if os.IsNotExist(e) {
					continue
				}
				ctx.PushErrorObjectVa(1, e.Error())
				ctx.PutPropString(idx, "error")
				break
			} else if info.IsDir() {
				continue
			}
			b, e := ioutil.ReadFile(filename)
			if e != nil {
				ctx.PushErrorObjectVa(1, e.Error())
				ctx.PutPropString(idx, "error")
				break
			}
			ctx.PushString(string(b))
			ctx.PutPropString(idx, "source")
			ctx.PushString(filename)
			ctx.PutPropString(idx, "filename")
			ctx.PushString(filepath.Dir(filename))
			ctx.PutPropString(idx, "dirname")
			ctx.PushString(id)
			ctx.PutPropString(idx, "id")
			break
		}
	}
	ctx.Pop2()
	return 1
}
func nativeLoad(ctx *duktape.Context) int {
	filename := ctx.GetString(0)
	ctx.Pop()
	ctx.PushObject()
	if !filepath.IsAbs(filename) {
		ctx.PushErrorObjectVa(duktape.ErrRetError, "not support module id : %s", filename)
		ctx.PutPropString(-2, "error")
		return 1
	}
	filename += ".js"
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		ctx.PushErrorObjectVa(duktape.ErrRetError, e.Error())
		ctx.PutPropString(-2, "error")
		return 1
	}
	ctx.PushString(string(b))
	ctx.PutPropString(-2, "source")
	ctx.PushString(filename)
	ctx.PutPropString(-2, "filename")
	ctx.PushString(filepath.Dir(filename))
	ctx.PutPropString(-2, "dirname")
	return 1
}
func nativeClean(ctx *duktape.Context) int {
	str := ctx.GetString(0)
	v := filepath.Clean(str)
	if v != str {
		ctx.Pop()
		ctx.PushString(v)
	}
	return 1
}
func nativeID(ctx *duktape.Context) int {
	from := ctx.GetString(0)
	name := ctx.GetString(1)
	id := filepath.Clean(filepath.Dir(from) + "/" + name)
	ctx.Pop2()
	ctx.PushString(id)
	return 1
}

func nativeCompile(ctx *duktape.Context) int {
	ctx.PushString("(function(require,module,exports,__dirname,__filename){")
	ctx.SwapTop(-2)
	ctx.PushString("\n})")
	ctx.Concat(3)
	ctx.SwapTop(-2)
	e := ctx.Pcompile(duktape.CompileEval)
	if e != nil {
		return 1
	}
	ctx.Pcall(0)
	return 1
}

// ExecuteJS 執行 js 代碼
func (ctx *Context) ExecuteJS(filename string) (e error) {
	if filepath.IsAbs(filename) {
		filename = filepath.Clean(filename)
	} else {
		filename, e = filepath.Abs(filename)
		if e != nil {
			return
		}
	}
	e = pevalFile(ctx.Context, filepath.Dir(filename), filename, false)
	return
}

// ExecuteJSNoresult 執行 js 代碼 不需要返回值
func (ctx *Context) ExecuteJSNoresult(filename string) (e error) {
	if filepath.IsAbs(filename) {
		filename = filepath.Clean(filename)
	} else {
		filename, e = filepath.Abs(filename)
		if e != nil {
			return
		}
	}
	e = pevalFile(ctx.Context, filepath.Dir(filename), filename, true)
	return
}
func pevalFile(ctx *duktape.Context, dirname, filename string, noresult bool) (e error) {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	ctx.GetGlobalString(GlobalModuleName)
	ctx.PushString("EvalFile")
	ctx.PushString(dirname)
	ctx.PushString(filename)
	ctx.PushString(string(b))
	if ctx.PcallProp(0, 3) != 0 {
		e = errors.New(ctx.SafeToString(-1))
	}
	if noresult {
		ctx.Pop2()
	} else {
		ctx.SwapTop(-2)
		ctx.Pop()
	}
	return
}

// RegisterNodeModule 註冊模塊 需要在 NodeInit 之後調用
//
// 需要將 一個 function(){} 的初始化 函數(第一次require時 被自動調用) 放入 duktape.Context 棧頂
//
// RegisterNodeModule 會保證 棧不變化 調用者需要 自己 釋放 duktape 棧中的參數
func (ctx *Context) RegisterNodeModule(id string) (e error) {
	if !ctx.IsFunction(-1) {
		e = fmt.Errorf("RegisterNodeModule need a function at top,not support %v", ctx.GetType(-1))
		return
	}
	if !ctx.GetGlobalString(GlobalModuleName) {
		e = errors.New("node module not init")
		ctx.Pop()
		return
	}
	ctx.PushString("RegisterNative")
	ctx.PushString(id)
	ctx.Dup(-4)
	if ctx.PcallProp(-4, 2) != 0 {
		e = errors.New(ctx.SafeToString(-1))
	}
	ctx.Pop2()
	return
}

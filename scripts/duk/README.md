# duk

go-duktape.v3 提供了 duktape 到 golang 的 c bind 使得 可以在golang中 方便的 嵌入 js

然 go-duktape.v3 主要是 將 c api 映射到 golang

duk 在此之上 對 golang 調用 go-duktape.v3 進行了 進一步的 客製化和簡化操作

# Context

```
// Context 包裝的 duktape 運行環境
type Context struct {
	*duktape.Context
}
```

Context 嵌入了 duktape.Context 可以 方便的 直接調用 duktape.Context api 同時 提供了 一些額外 功能

# node module

`func (ctx *Context) NodeInit(root []string) (e error)` 將爲 duktape 初始化一個 全局變量 GlobalModuleName 

```
// GlobalModuleName 全局模塊名
const GlobalModuleName = "__king_duktape_node_modules"
```
GlobalModuleName 提供了 部分 兼容 node 模塊的功能

```
func main() {
	ctx := Context{
		duktape.New(),
	}
	ctx.NodeInit([]string{"."})

	e := RegisterRuntimeForNode(ctx)
	if e != nil {
		log.Fatalln(e)
	}

	e = ctx.ExecuteJS("src/main.js")
	if e != nil {
		log.Println(e)
	}
}
```

## RegisterNodeModule 函數 提供了 註冊 golang 模塊的 方法

```
package duk

import (
	"runtime"

	"gopkg.in/olebedev/go-duktape.v3"
)

// RegisterRuntimeForNode 使用 node 風格 註冊 runtime 模塊
func RegisterRuntimeForNode(ctx Context) (e error) {
	// 傳入 模塊 初始化函數
	ctx.PushGoFunction(nativeRuntime)
	// 註冊模塊
	e = ctx.RegisterNodeModule("runtime")
	// 清除 棧 參數
	ctx.Pop()
	return
}
func nativeRuntime(ctx *duktape.Context) int {
	// 設置 初始化 返回值
	idx := ctx.PushObject()

	// error 如果 失敗 設置 error 返回 錯誤
	// ctx.PushErrorObjectVa(duktape.ErrRetError, "error test")
	// ctx.PutPropString(idx, "error")

	// exports 將 模塊 導出 符號設置到 exports
	{
		ctx.PushObject()
		ctx.PushString(runtime.GOOS)
		ctx.PutPropString(-2, "os")
		ctx.PushString(runtime.GOARCH)
		ctx.PutPropString(-2, "arch")
		ctx.PushString(runtime.Version())
		ctx.PutPropString(-2, "version")
	}
	ctx.PutPropString(idx, "exports")

	return 1
}
```

# 注意

不要在 go 中 調用 throw 拋出異常到 duktape 中 程式會直接 崩潰

同樣 不要 在go中調用 可能引發 throw 的 函數 
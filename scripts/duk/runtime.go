package duk

import (
	"runtime"

	"gopkg.in/olebedev/go-duktape.v3"
)

// RegisterRuntimeForNode 使用 node 風格 註冊 runtime 模塊
func RegisterRuntimeForNode(ctx Context) (e error) {
	// 傳入 模塊 初始化函數
	ctx.PushGoFunction(nativeRuntime)
	// 註冊模塊
	e = ctx.RegisterNodeModule("runtime")
	// 清除 棧 參數
	ctx.Pop()
	return
}
func nativeRuntime(ctx *duktape.Context) int {
	// 設置 初始化 返回值
	idx := ctx.PushObject()

	// error 如果 失敗 設置 error 返回 錯誤
	// ctx.PushErrorObjectVa(duktape.ErrRetError, "error test")
	// ctx.PutPropString(idx, "error")

	// exports 將 模塊 導出 符號設置到 exports
	{
		ctx.PushObject()
		ctx.PushString(runtime.GOOS)
		ctx.PutPropString(-2, "os")
		ctx.PushString(runtime.GOARCH)
		ctx.PutPropString(-2, "arch")
		ctx.PushString(runtime.Version())
		ctx.PutPropString(-2, "version")
	}
	ctx.PutPropString(idx, "exports")

	return 1
}

package log

import (
	"github.com/fatih/color"
	"log"
	"sync"
)

// Trace .
var Trace *log.Logger

// Debug .
var Debug *log.Logger

// Info .
var Info *log.Logger

// Warn .
var Warn *log.Logger

// Error .
var Error *log.Logger

// Fault .
var Fault *log.Logger

// InitDebugLoggers 初始化 所有 全局 日誌 以便調試
func InitDebugLoggers() {
	c := NewCreator()
	c.Color = false
	m := &sync.Mutex{}
	Trace = c.NewTrace(NewStdoutColorWriter(color.New(color.FgCyan), m))
	Debug = c.NewDebug(NewStdoutColorWriter(color.New(color.FgBlue), m))
	Info = c.NewInfo(NewStdoutColorWriter(color.New(color.FgGreen), m))
	Warn = c.NewWarn(NewStdoutColorWriter(color.New(color.FgYellow), m))
	Error = c.NewError(NewStdoutColorWriter(color.New(color.FgMagenta), m))
	Fault = c.NewFault(NewStdoutColorWriter(color.New(color.FgRed), m))
}

// Package logger 爲 zap 提供了 一些包裝
package logger

import (
	"errors"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"net"
	"net/http"
	"os"
	"strings"
)

// ErrOptionsHTTPNotSet .
var ErrOptionsHTTPNotSet = errors.New("Options.HTTP not set")

// ErrListenerHTTPNotWork .
var ErrListenerHTTPNotWork = errors.New("Listener HTTP not work")

// ErrListenerHTTPAlreadyWork .
var ErrListenerHTTPAlreadyWork = errors.New("Listener HTTP already work")

// Options 日誌 選項
type Options struct {
	// 日誌 http 如果爲空 則不啓動 http
	HTTP string `json:"HTTP"`
	// 日誌 檔案名 如果爲空 則 輸出到 控制檯
	Filename string
	// 單個日誌檔案 大小上限 MB
	MaxSize int
	// 保存 多少個 日誌 檔案
	MaxBackups int
	// 保存 多少天內的 日誌
	MaxAge int
	// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
	Level string
	// 是否要 輸出 代碼位置
	Caller bool
}

// Logger zap 日誌
type Logger struct {
	// zap 記錄器
	*zap.Logger
	atom zap.AtomicLevel

	// 輸入 日誌 檔案名
	filename string

	// http 工作地址
	http         string
	listenerHTTP net.Listener
}

// Attach 附加到 已存在的 日誌記錄器
func (l *Logger) Attach(src *Logger) {
	l.Logger = src.Logger
	l.atom = src.atom
	l.filename = src.filename
	l.http = src.http
	l.listenerHTTP = src.listenerHTTP
}

// Detach 分離 Logger
func (l *Logger) Detach() {
	l.Logger = nil
	l.atom = zap.AtomicLevel{}
	l.filename = ""
	l.http = ""
	l.listenerHTTP = nil
}

// New 創建 一個 日誌記錄器
//
// 如果 options 爲空 則 創建一個 默認的 記錄器
func New(options *Options, zapOptions ...zap.Option) *Logger {
	// 設置 日誌 記錄 等級
	atom := zap.NewAtomicLevel()
	if options == nil {
		atom.SetLevel(zapcore.InfoLevel)
	} else {
		switch options.Level {
		case "debug":
			atom.SetLevel(zapcore.DebugLevel)
		case "info":
			atom.SetLevel(zapcore.InfoLevel)
		case "warn":
			atom.SetLevel(zapcore.WarnLevel)
		case "error":
			atom.SetLevel(zapcore.ErrorLevel)
		case "dpanic":
			atom.SetLevel(zapcore.DPanicLevel)
		case "panic":
			atom.SetLevel(zapcore.PanicLevel)
		case "fatal":
			atom.SetLevel(zapcore.FatalLevel)
		}
	}

	// 設置 輸出 目標
	var filename string
	if options != nil {
		filename = strings.TrimSpace(options.Filename)
	}
	var core zapcore.Core
	if filename == "" {
		encoderCfg := zap.NewDevelopmentEncoderConfig()
		core = zapcore.NewCore(
			zapcore.NewConsoleEncoder(encoderCfg),
			os.Stdout,
			atom,
		)
	} else {
		// 創建 日誌 輸出 檔案
		w := zapcore.AddSync(&lumberjack.Logger{
			Filename:   filename,
			MaxSize:    options.MaxSize, // megabytes
			MaxBackups: options.MaxBackups,
			MaxAge:     options.MaxAge, // days
		})
		encoderCfg := zap.NewProductionEncoderConfig()
		core = zapcore.NewCore(
			zapcore.NewJSONEncoder(encoderCfg),
			w,
			atom,
		)
	}

	if options != nil && options.Caller {
		zapOptions = append(zapOptions, zap.AddCaller())
	}
	// 創建 記錄器
	logger := zap.New(core,
		zapOptions...,
	)

	return &Logger{
		logger, atom,
		filename,
		strings.TrimSpace(options.HTTP),
		nil,
	}
}

// OutFile 返回 日誌是否 記錄到 檔案
func (l *Logger) OutFile() bool {
	return l.filename != ""
}

// OutConsole 返回 日誌是否 記錄到 控制檯
func (l *Logger) OutConsole() bool {
	return l.filename == ""
}

// HTTP 返回 HTTP 工作地址
func (l *Logger) HTTP() string {
	return l.http
}

// StartHTTP 運行 http 服務器
func (l *Logger) StartHTTP() (e error) {
	if l.listenerHTTP != nil {
		e = ErrListenerHTTPAlreadyWork
		return
	} else if l.http == "" {
		e = ErrOptionsHTTPNotSet
		return
	}

	// 運行 http 服務
	srv := http.Server{Handler: l.atom}
	var listen net.Listener
	listen, e = net.Listen("tcp", l.http)
	if e == nil {
		l.listenerHTTP = listen
		go srv.Serve(listen)
	}
	return
}

// StopHTTP 停止 http 服務器
func (l *Logger) StopHTTP() (e error) {
	if l.listenerHTTP == nil {
		e = ErrListenerHTTPNotWork
		return
	}
	e = l.listenerHTTP.Close()
	if e == nil {
		l.listenerHTTP = nil
	}
	return
}

// ListenerHTTP 返回 HTTP net.Listener
func (l *Logger) ListenerHTTP() net.Listener {
	return l.listenerHTTP
}

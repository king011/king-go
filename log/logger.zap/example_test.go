package logger_test

import (
	"gitlab.com/king011/king-go/log/logger.zap"
	"go.uber.org/zap"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func ExampleJoinFields() {
	l := logger.New(nil, nil)
	if ce := l.Check(zap.InfoLevel, "info test"); ce != nil {
		ce.Write(
			logger.JoinFields(
				logger.Fields(zap.String("name", "illusive man"), zap.String("company", "cerberus")),
				logger.Fields(zap.String("love", "kate beckinsale"), zap.Int("lv", 6)),
			)...,
		)
	}
}

func ExampleLogger() {
	// Logger 日誌 單件
	var Logger logger.Logger
	//var JoinFields = logger.JoinFields
	//var Fields = logger.Fields

	// find baseURL
	baseURL, e := exec.LookPath(os.Args[0])
	if e != nil {
		log.Fatalln(e)
	}
	baseURL, e = filepath.Abs(baseURL)
	if e != nil {
		log.Fatalln(e)
	}

	// format options
	options := &logger.Options{}
	options.Filename = strings.TrimSpace(options.Filename)
	if options.Filename != "" {
		if !filepath.IsAbs(options.Filename) {
			options.Filename = filepath.Clean(baseURL + "/" + options.Filename)
		}
	}
	var zapOptions []zap.Option
	if options.Caller {
		zapOptions = append(zapOptions, zap.AddCaller())
	}

	// new zap logger
	l := logger.New(options, zapOptions...)

	// run http
	if options.HTTP != "" {
		errHTTP := l.StartHTTP()
		if errHTTP == nil {
			if l.OutFile() {
				log.Println("zap http running", options.HTTP)
			}
			l.Info("zap http",
				zap.String("running", options.HTTP),
			)
		} else {
			if l.OutFile() {
				log.Println("zap http running", errHTTP)
			}
			l.Warn("zap http",
				zap.Error(errHTTP),
			)
		}
	}
	// Attach
	Logger.Attach(l)
}

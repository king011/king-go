package log

import (
	"fmt"
	"github.com/fatih/color"
	"io"
	"log"
	"sync"
)

// Loggers .
type Loggers struct {
	Trace, Debug, Info, Warn, Error, Fault *log.Logger
}

func (l *Loggers) out(w *log.Logger, v ...interface{}) {
	if w == nil {
		return
	}
	switch len(v) {
	case 0:
		return
	default:
		if format, ok := v[0].(string); ok {
			if format == "" {
				w.Output(3, fmt.Sprintln(v...))
			} else {
				w.Output(3, fmt.Sprintf(format, v[1:]...))
			}
		} else {
			w.Output(3, fmt.Sprintln(v...))
		}
	}
	return
}

// Tracef .
func (l *Loggers) Tracef(v ...interface{}) {
	l.out(l.Trace, v...)
}

// Debugf .
func (l *Loggers) Debugf(v ...interface{}) {
	l.out(l.Debug, v...)
}

// Infof .
func (l *Loggers) Infof(v ...interface{}) {
	l.out(l.Info, v...)
}

// Warnf .
func (l *Loggers) Warnf(v ...interface{}) {
	l.out(l.Warn, v...)
}

// Errorf .
func (l *Loggers) Errorf(v ...interface{}) {
	l.out(l.Error, v...)
}

// Faultf .
func (l *Loggers) Faultf(v ...interface{}) {
	l.out(l.Fault, v...)
}

// SetFlags .
func (l *Loggers) SetFlags(flag int) {
	if l.Trace != nil {
		l.Trace.SetFlags(flag)
	}
	if l.Debug != nil {
		l.Debug.SetFlags(flag)
	}
	if l.Info != nil {
		l.Info.SetFlags(flag)
	}
	if l.Warn != nil {
		l.Warn.SetFlags(flag)
	}
	if l.Error != nil {
		l.Error.SetFlags(flag)
	}
	if l.Fault != nil {
		l.Fault.SetFlags(flag)
	}
}

// NewLoggers 創建默認的 日誌
func NewLoggers(out io.Writer, flags int) *Loggers {
	c := NewCreator()
	c.Flags = flags
	return &Loggers{
		Info:  c.NewInfo(out),
		Warn:  c.NewWarn(out),
		Error: c.NewError(out),
		Fault: c.NewFault(out),
	}
}

// NewDebugLoggers 初始化 默認 調試 日誌
func NewDebugLoggers() *Loggers {
	c := NewCreator()
	c.Color = false
	m := &sync.Mutex{}

	return &Loggers{
		Trace: c.NewTrace(NewStdoutColorWriter(color.New(color.FgCyan), m)),
		Debug: c.NewDebug(NewStdoutColorWriter(color.New(color.FgBlue), m)),
		Info:  c.NewInfo(NewStdoutColorWriter(color.New(color.FgGreen), m)),
		Warn:  c.NewWarn(NewStdoutColorWriter(color.New(color.FgYellow), m)),
		Error: c.NewError(NewStdoutColorWriter(color.New(color.FgMagenta), m)),
		Fault: c.NewFault(NewStdoutColorWriter(color.New(color.FgRed), m)),
	}
}

// NewDebugLoggers2 初始化 默認 調試 日誌
func NewDebugLoggers2(tag string) *Loggers {
	c := NewCreator()
	c.Tag = tag
	c.Color = false
	m := &sync.Mutex{}

	return &Loggers{
		Trace: c.NewTrace(NewStdoutColorWriter(color.New(color.FgCyan), m)),
		Debug: c.NewDebug(NewStdoutColorWriter(color.New(color.FgBlue), m)),
		Info:  c.NewInfo(NewStdoutColorWriter(color.New(color.FgGreen), m)),
		Warn:  c.NewWarn(NewStdoutColorWriter(color.New(color.FgYellow), m)),
		Error: c.NewError(NewStdoutColorWriter(color.New(color.FgMagenta), m)),
		Fault: c.NewFault(NewStdoutColorWriter(color.New(color.FgRed), m)),
	}
}

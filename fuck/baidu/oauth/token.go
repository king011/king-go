package oauth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Error 錯誤
type Error struct {
	Message     string `json:"error"`
	Description string `json:"error_description"`
}

func (e Error) Error() string {
	return e.Description
}

// Token 授權
type Token struct {
	AccessToken   string `json:"access_token"`
	ExpiresIn     int64  `json:"expires_in"`
	RefreshToken  string `json:"refresh_token"`
	Scope         string `json:"scope"`
	SessionKey    string `json:"session_key"`
	SessionSecret string `json:"session_secret"`
}

// NewToken 申請一個 授權
func NewToken(ctx context.Context, apiKey, secretKey string) (token *Token, e error) {
	// 創建 http 請求
	url := fmt.Sprintf(`https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s`,
		apiKey,
		secretKey,
	)
	var request *http.Request
	request, e = http.NewRequest("GET", url, nil)
	if e != nil {
		return
	}
	request = request.WithContext(ctx)
	var client http.Client
	var response *http.Response
	response, e = client.Do(request)
	if e != nil {
		return
	}

	// 解析 數據
	var b []byte
	b, e = ioutil.ReadAll(response.Body)
	response.Body.Close()
	if e != nil {
		return
	}
	if response.StatusCode == 200 {
		var tk Token
		e = json.Unmarshal(b, &tk)
		if e != nil {
			return
		}
		token = &tk
	} else {
		var rs Error
		e = json.Unmarshal(b, &rs)
		if e == nil {
			e = rs
		}
	}
	return
}

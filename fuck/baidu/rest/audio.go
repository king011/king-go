package rest

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"gitlab.com/king011/king-go/fuck/baidu/oauth"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

const (
	text2audioURL = `https://tsn.baidu.com/text2audio`
)

// Text2Audio 文字 轉語音
type Text2Audio struct {
	// 要轉換的文本 小於 2048 轉 GBK 後 小於 4096
	// Text string
	// Token.AccessToken
	AccessToken string
	// 用來 計算 UV 的字符串 通常是 MAC 或 IMEI 最大 60 字符
	UV string

	// 客戶端 類型 web 固定爲 1
	Client int

	// 語言 固定爲 zh
	Language string

	// 語速 [0-15] 5
	Speed int

	// 音調 [0-15] 5
	Pitch int
	// 音量 [0-15] 5
	Volume int
	// 發音人
	// 0 为普通女声
	// 1 为普通男生
	// 3 为情感合成-度逍遥
	// 4 为情感合成-度丫丫
	// 默认为普通女声
	Person int
	// 音頻格式
	// 3 为 mp3格式(默认)
	// 4 为 pcm-16k
	// 5 为 pcm-8k
	// 6 为 wav（内容同pcm-16k）
	Aue int
}

// NewText2Audio 創建一個 默認的 語音轉換 設置
func NewText2Audio() *Text2Audio {
	return &Text2Audio{
		UV:       "uv",
		Client:   1,
		Language: "zh",
		Speed:    5,
		Pitch:    5,
		Volume:   5,
		Person:   0,
		Aue:      3,
	}
}

// Request 發送一個 轉換請求
func (t *Text2Audio) Request(ctx context.Context, token *oauth.Token, text string) (r io.ReadCloser, content string, e error) {
	// body
	text = url.QueryEscape(text)
	text = url.QueryEscape(text)
	str := fmt.Sprintf(`tex=%v&tok=%v&cuid=%v&ctp=%v&lan=%v&spd=%v&pit=%v&vol=%v&per=%v&aue=%v`,
		text,
		token.AccessToken,
		t.UV,
		t.Client,
		t.Language,
		t.Speed,
		t.Pitch,
		t.Volume,
		t.Person,
		t.Aue,
	)
	body := bytes.NewBufferString(str)

	// 創建 http 請求
	var request *http.Request
	request, e = http.NewRequest("POST", text2audioURL, body)
	if e != nil {
		return
	}
	request = request.WithContext(ctx)
	var client http.Client
	var response *http.Response
	response, e = client.Do(request)
	if e != nil {
		return
	}

	if response.StatusCode == 200 {
		contentType := response.Header.Get("Content-Type")
		if strings.HasPrefix(contentType, "audio/") {
			content = contentType[6:]
			r = response.Body
		} else {
			var b []byte
			b, e = ioutil.ReadAll(response.Body)
			response.Body.Close()
			e = errors.New(string(b))
		}
	} else {
		var b []byte
		b, e = ioutil.ReadAll(response.Body)
		response.Body.Close()
		e = errors.New(string(b))
	}
	return
}

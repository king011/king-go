package io

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// InputReader 獲取 用戶 輸入
type InputReader struct {
	In      *bufio.Reader
	Out     io.Writer
	Err     io.Writer
	OutLine int
	ErrLine int
}

// NewInputReader 從 os.Stdin os.Stdout os.Stderr  創建 用戶 輸入 reader
func NewInputReader() *InputReader {
	return &InputReader{
		In:  bufio.NewReader(os.Stdin),
		Out: os.Stdout,
		Err: os.Stderr,
	}
}

// NewInputReader3 創建 用戶 輸入 reader
func NewInputReader3(r io.Reader, out, err io.Writer) *InputReader {
	if r == nil {
		panic("reader nil")
	} else if out == nil {
		panic("out writer nil")
	} else if err == nil {
		panic("err writer nil")
	}
	return &InputReader{
		In:  bufio.NewReader(r),
		Out: out,
		Err: err,
	}
}

// ReadString 等待用戶 輸入字符串
func (r *InputReader) ReadString(placeholder string) (val string) {
	for {
		fmt.Fprintf(r.Out, "%s $ ", placeholder)
		b, _, e := r.In.ReadLine()
		if e != nil {
			fmt.Fprintln(r.Err, e)
			continue
		}
		val = strings.TrimSpace(string(b))
		if val != "" {
			break
		}
	}
	return
}

// ReadStringDefault 等待用戶 輸入字符串
func (r *InputReader) ReadStringDefault(placeholder, def string) (val string) {
	for {
		fmt.Fprintf(r.Out, "%s [%v] $ ", placeholder, def)
		b, _, e := r.In.ReadLine()
		if e != nil {
			fmt.Fprintln(r.Err, e)
			continue
		}
		val = strings.TrimSpace(string(b))
		if val == "" {
			val = def
		}
		break
	}
	return
}

// ReadNumber 等待用戶 輸入 數字
func (r *InputReader) ReadNumber(placeholder string) (val int64) {
	for {
		fmt.Fprintf(r.Out, "%s $ ", placeholder)
		b, _, e := r.In.ReadLine()
		if e != nil {
			r.Eprintln(e)
			continue
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		val, e = strconv.ParseInt(str, 10, 64)
		if e != nil {
			r.Eprintln(e)
			continue
		}
		break
	}
	return
}

// ReadNumberDefault 等待用戶 輸入 數字
func (r *InputReader) ReadNumberDefault(placeholder string, def int64) (val int64) {
	for {
		fmt.Fprintf(r.Out, "%s [%v] $ ", placeholder, def)
		b, _, e := r.In.ReadLine()
		if e != nil {
			r.Eprintln(e)
			continue
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "" {
			val = def
			break
		}
		val, e = strconv.ParseInt(str, 10, 64)
		if e != nil {
			r.Eprintln(e)
			continue
		}
		break
	}
	return
}

// ReadBool 等待用戶 輸入 bool
func (r *InputReader) ReadBool(placeholder string) (val bool) {
	for {
		fmt.Fprintf(r.Out, "%s $ ", placeholder)
		b, _, e := r.In.ReadLine()
		if e != nil {
			fmt.Fprintln(r.Err, e)
			continue
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "true" || str == "t" ||
			str == "yes" || str == "y" ||
			str == "1" {
			val = true
			break
		} else if str == "false" || str == "f" ||
			str == "no" || str == "n" ||
			str == "0" {
			val = false
			break
		}
	}
	return
}

// ReadBoolDefault 等待用戶 輸入 bool
func (r *InputReader) ReadBoolDefault(placeholder string, def bool) (val bool) {
	for {
		fmt.Fprintf(r.Out, "%s [%v] $ ", placeholder, def)
		b, _, e := r.In.ReadLine()
		if e != nil {
			fmt.Fprintln(r.Err, e)
			continue
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "true" || str == "t" ||
			str == "yes" || str == "y" ||
			str == "1" {
			val = true
			break
		} else if str == "false" || str == "f" ||
			str == "no" || str == "n" ||
			str == "0" {
			val = false
			break
		}
		val = def
		break
	}
	return
}

// Print fmt.Fprint(r.Out, a...)
func (r *InputReader) Print(a ...interface{}) (n int, err error) {
	w := &_WriterB{
		writer: r.Out,
	}
	n, err = fmt.Fprint(w, a...)
	r.OutLine = w.n
	return
}
func (r *InputReader) outLine(a ...interface{}) {
	if r.OutLine != 0 {
		fmt.Fprintf(r.Out, fmt.Sprintf("\r%%-%ds\r", r.OutLine), "")
		r.OutLine = 0
	}
}

// PrintLine \r and fmt.Fprint(r.Out, a...)
func (r *InputReader) PrintLine(a ...interface{}) (n int, err error) {
	r.outLine()
	n, err = r.Print(a...)
	return
}

// Println fmt.Fprintln(r.Out, a...)
func (r *InputReader) Println(a ...interface{}) (n int, err error) {
	w := &_WriterB{
		writer: r.Out,
	}
	n, err = fmt.Fprintln(w, a...)
	r.OutLine = w.n
	return
}

// PrintlnLine \r and fmt.Fprintln(r.Out, a...)
func (r *InputReader) PrintlnLine(a ...interface{}) (n int, err error) {
	r.outLine()
	n, err = r.Println(a...)
	return
}

// Printf fmt.Fprintln(r.Out, a...)
func (r *InputReader) Printf(format string, a ...interface{}) (n int, err error) {
	w := &_WriterB{
		writer: r.Out,
	}
	n, err = fmt.Fprintf(w, format, a...)
	r.OutLine = w.n
	return
}

// PrintfLine \r and fmt.Fprintf(r.Out, a...)
func (r *InputReader) PrintfLine(format string, a ...interface{}) (n int, err error) {
	r.outLine()
	n, err = r.Printf(format, a...)
	return
}
func (r *InputReader) errLine(a ...interface{}) {
	if r.ErrLine != 0 {
		fmt.Fprintf(r.Err, fmt.Sprintf("\r%%-%ds\r", r.ErrLine), "")
		r.ErrLine = 0
	}
}

// Eprint fmt.Fprint(r.Err, a...)
func (r *InputReader) Eprint(a ...interface{}) (n int, err error) {
	w := &_WriterB{
		writer: r.Err,
	}
	n, err = fmt.Fprint(w, a...)
	r.ErrLine = w.n
	return
}

// EprintLine \r and fmt.Fprint(r.Err, a...)
func (r *InputReader) EprintLine(a ...interface{}) (n int, err error) {
	r.outLine()
	n, err = r.Eprint(a...)
	return
}

// Eprintln fmt.Fprintln(r.Err, a...)
func (r *InputReader) Eprintln(a ...interface{}) (n int, err error) {
	w := &_WriterB{
		writer: r.Err,
	}
	n, err = fmt.Fprintln(w, a...)
	r.ErrLine = w.n
	return
}

// EprintlnLine \n and  fmt.Fprintln(r.Err, a...)
func (r *InputReader) EprintlnLine(a ...interface{}) (n int, err error) {
	r.outLine()
	n, err = r.Eprintln(a...)
	return
}

// Eprintf fmt.Fprintln(r.Err, a...)
func (r *InputReader) Eprintf(format string, a ...interface{}) (n int, err error) {
	w := &_WriterB{
		writer: r.Err,
	}
	n, err = fmt.Fprintf(w, format, a...)
	r.ErrLine = w.n
	return
}

// EprintfLine \n and fmt.Fprintln(r.Err, a...)
func (r *InputReader) EprintfLine(format string, a ...interface{}) (n int, err error) {
	r.outLine()
	n, err = r.Eprintf(format, a...)
	return
}

type _WriterB struct {
	writer io.Writer
	n      int
}

func (w *_WriterB) Write(b []byte) (n int, err error) {
	n, err = w.writer.Write(b)
	if n != 0 {
		b = b[:n]
		find := bytes.LastIndexByte(b, '\n')
		if find == -1 {
			w.n += n
		} else {
			w.n += n - find - 1
		}
	}
	return
}

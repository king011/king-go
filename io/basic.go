package io

import (
	"io"
)

type _Reader struct {
	io.Reader
}

// Reader 包裝 io.Reader 保證 其只 實現了 io.Reader 接口
func Reader(src io.Reader) io.Reader {
	return _Reader{src}
}

type _ReadCloser struct {
	io.ReadCloser
}

// ReadCloser 包裝 io.ReadCloser 保證 其只 實現了 io.ReadCloser 接口
func ReadCloser(src io.ReadCloser) io.ReadCloser {
	return _ReadCloser{src}
}

type _Writer struct {
	io.Writer
}

// Writer 包裝 io.Writer 保證其只 實現了 io.Writer 接口
func Writer(src io.Writer) io.Writer {
	return _Writer{src}
}

type _WriteCloser struct {
	io.WriteCloser
}

// WriteCloser 包裝 io.WriteCloser 保證其只 實現了 io.WriteCloser 接口
func WriteCloser(src io.WriteCloser) io.WriteCloser {
	return _WriteCloser{src}
}

type _ReadWriteCloser struct {
	io.ReadWriteCloser
}

// ReadWriteCloser 包裝 io.ReadWriteCloser 保證其只 實現了 io.ReadWriteCloser 接口
func ReadWriteCloser(src io.ReadWriteCloser) io.ReadWriteCloser {
	return _ReadWriteCloser{src}
}

package message

import (
	"io"
)

// BasicParser 一個 基礎的 消息 解析器
type BasicParser struct {
	Analyst Analyst
}

// ReadMessage 從 io.Reader 中 讀出一個 消息
func (p BasicParser) ReadMessage(r io.Reader) (msg []byte, e error) {
	analyst := p.Analyst
	headerLen := analyst.Header()

	var header []byte
	if headerLen > 0 {
		var pos, n int
		// 讀取 header
		header = make([]byte, headerLen)
		for pos != headerLen {
			n, e = r.Read(header[pos:])
			if e != nil {
				return
			}
			pos += n
		}
	} else if headerLen < 0 {
		headerLen = 0
	}
	// 返回 body 長度
	bodyLen, e := analyst.Body(header)
	if e != nil {
		return
	} else if bodyLen < 1 {
		msg = header
		return
	}

	// 讀取 body
	size := headerLen + bodyLen
	b := make([]byte, size)
	var n int
	pos := headerLen
	for pos != size {
		n, e = r.Read(b[pos:])
		if e != nil {
			return
		}
		pos += n
	}

	// 返回 消息
	copy(b, header)
	msg = b
	return
}

// BufferParser 一個 帶讀取 緩衝區的 消息 解析器
type BufferParser struct {
	Analyst Analyst
	Buffer  []byte
	Pos     int
	Size    int
}

// ReadMessage 從 io.Reader 中 讀出一個 消息
func (p *BufferParser) ReadMessage(r io.Reader) (msg []byte, e error) {
	analyst := p.Analyst
	headerLen := analyst.Header()
	var header []byte
	if headerLen > 0 {
		var pos, n int
		// 讀取 header
		header = make([]byte, headerLen)
		for pos != headerLen {
			n, e = p.read(r, header[pos:])
			if e != nil {
				return
			}
			pos += n
		}
	} else if headerLen < 0 {
		headerLen = 0
	}

	// 返回 body 長度
	bodyLen, e := analyst.Body(header)
	if e != nil {
		return
	} else if bodyLen < 1 {
		msg = header
		return
	}

	// 讀取 body
	size := headerLen + bodyLen
	b := make([]byte, size)
	var n int
	pos := headerLen
	for pos != size {
		n, e = p.read(r, b[pos:])
		if e != nil {
			return
		}
		pos += n
	}

	// 返回 消息
	copy(b, header)
	msg = b
	return
}
func (p *BufferParser) read(r io.Reader, buf []byte) (n int, e error) {
	size := len(buf)
	for size == 0 {
		return
	}
	cache, e := p.getCache(r)
	if e != nil {
		return
	}
	n = copy(buf, cache)
	p.Pos += n
	p.Size -= n
	size -= n
	return
}
func (p *BufferParser) getCache(r io.Reader) (cache []byte, e error) {
	size := p.Size
	if size != 0 {
		cache = p.Buffer[p.Pos : p.Pos+size]
		return
	}

	n, e := r.Read(p.Buffer)
	if e != nil {
		return
	}
	if n != 0 {
		p.Pos = 0
		p.Size = n
	}
	return
}

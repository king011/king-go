package message_test

import (
	"bytes"
	"encoding/binary"
	"io"
	"testing"

	"gitlab.com/king011/king-go/io/message"
)

func done(t *testing.T, parser message.Parser) {
	// write
	str := "kate"
	b, e := message.Marshal(1, []byte(str))
	if e != nil {
		t.Fatal(e)
	}
	rw := bytes.NewBuffer(b)

	str = "illusive man"
	b, e = message.Marshal(2, []byte(str))
	if e != nil {
		t.Fatal(e)
	}
	_, e = rw.Write(b)
	if e != nil {
		t.Fatal(e)
	}

	// read
	msg, e := parser.ReadMessage(rw)
	if e != nil {
		t.Fatal(e)
	}
	cmd := binary.LittleEndian.Uint16(msg[2:])
	if cmd != 1 {
		t.Fatalf("bad cmd [%v] != 1", cmd)
	}
	str = string(msg[4:])
	if str != "kate" {
		t.Fatalf("bad body [%v] != kate", str)
	}

	msg, e = parser.ReadMessage(rw)
	if e != nil {
		t.Fatal(e)
	}
	cmd = binary.LittleEndian.Uint16(msg[2:])
	if cmd != 2 {
		t.Fatalf("bad cmd [%v] != 2", cmd)
	}
	str = string(msg[4:])
	if str != "illusive man" {
		t.Fatalf("bad body [%v] != illusive man", str)
	}

	_, e = parser.ReadMessage(rw)
	if e != io.EOF {
		t.Fatalf("bad error [%v] != io.EOF", e)
	}
}

func TestParser(t *testing.T) {
	parser := message.NewParser(message.NewDefaultAnalyst())
	done(t, parser)
}
func TestBufferParser(t *testing.T) {
	for i := 0; i < 50; i++ {
		parser := message.NewBufferParser(
			message.NewDefaultAnalyst(),
			make([]byte, i),
		)
		done(t, parser)
	}
}

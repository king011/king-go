// Package message 定義了 如何從 io.Reader 中 解析消息
package message

import "io"

// Analyst 是一個 消息 分析者
type Analyst interface {
	// Header 返回 消息 包頭 長度
	Header() int
	// Body 傳入 包頭 返回 body 長度
	Body(header []byte) (int, error)
}

// AnalyzeFunc 由 header 分析 body
type AnalyzeFunc func(header []byte) (int, error)

// NewAnalyst 創建一個 消息 分析者
func NewAnalyst(header int, f AnalyzeFunc) Analyst {
	return &_Analyst{
		header: header,
		body:   f,
	}
}

// NewDefaultAnalyst 返回 默認的 消息 分析者
//
// 由 Header{} + Body {} 組成的消息
//
// Header 由 binary.LittleEndian 16 字節 body長度 + 16 字節 指令碼 組成
func NewDefaultAnalyst() Analyst {
	return _DefaultAnalyst{}
}

// Parser 是一個 消息 解析器
type Parser interface {
	// ReadMessage 從 io.Reader 中 讀出一個 消息
	ReadMessage(r io.Reader) (msg []byte, e error)
}

// NewParser 創建一個 消息 解析器
func NewParser(analyst Analyst) (parser Parser) {
	return BasicParser{Analyst: analyst}
}

// NewBufferParser 創建一個 帶讀取緩衝區的 消息 解析器 *BufferParser
//
// 如果 len(buffer) == 0 則 返回 BasicParser
func NewBufferParser(analyst Analyst, buffer []byte) (parser Parser) {
	if len(buffer) == 0 {
		return BasicParser{Analyst: analyst}
	}
	return &BufferParser{
		Analyst: analyst,
		Buffer:  buffer,
	}
}

package message

// 定義了 各種 錯誤 代碼
const (
	// 數據 超過了允許的 最大 長度
	ErrOutOfMaxData = iota + 1
)

// Error 錯誤 信息
type Error struct {
	code    int
	message string
}

// Error 返回 錯誤 描述信息
func (e Error) Error() string {
	return e.message
}

// Code 返回 錯誤 代碼
func (e Error) Code() int {
	return e.code
}

// IsOutOfMaxData 如果 e 是 ErrOutOfMaxData 返回 true
func IsOutOfMaxData(e error) bool {
	if v, ok := e.(Error); ok {
		return v.code == ErrOutOfMaxData
	}
	return false
}

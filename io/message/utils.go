package message

import (
	"encoding/binary"
	"fmt"
	"math"
)

// Marshal 創建 默認的 消息
//
// 由 Header{} + Body {} 組成的消息
//
// Header 由 binary.LittleEndian 16 字節 body長度 + 16 字節 指令碼 組成
//
// 失敗 返回 ErrOutOfMaxData
func Marshal(cmd uint16, data []byte) (msg []byte, e error) {
	bodyLen := len(data)
	if bodyLen > math.MaxUint16 {
		e = Error{
			code:    ErrOutOfMaxData,
			message: fmt.Sprintf("Out of max data length,%v > %v", bodyLen, math.MaxUint16),
		}
		return
	}
	msg = make([]byte, 4+bodyLen)
	binary.LittleEndian.PutUint16(msg, uint16(bodyLen))
	binary.LittleEndian.PutUint16(msg[2:], cmd)
	copy(msg[2+2:], data)
	return
}

// MarshalString Marshal 的 語法糖
func MarshalString(cmd uint16, data string) (msg []byte, e error) {
	if len(data) == 0 {
		return Marshal(cmd, nil)
	}
	return Marshal(cmd, []byte(data))
}

// MarshalError Marshal 的 語法糖
func MarshalError(cmd uint16, data error) (msg []byte, e error) {
	if data == nil {
		return Marshal(cmd, nil)
	}
	return MarshalString(cmd, data.Error())
}

// Ok 如果 msg 是 默認 消息包 返回 true
func Ok(msg []byte) bool {
	if len(msg) < 4 {
		return false
	}
	return binary.LittleEndian.Uint16(msg) == uint16(len(msg)-4)
}

// Command 返回 默認 消息包 指令碼
func Command(msg []byte) uint16 {
	return binary.LittleEndian.Uint16(msg[2:])
}

// SetCommand 設置 默認 消息包 的 指令碼
func SetCommand(msg []byte, cmd uint16) {
	binary.LittleEndian.PutUint16(msg[2:], cmd)
}

// Body 返回 默認 消息包 body
func Body(msg []byte) []byte {
	return msg[4:]
}

// Format 填充 默認 消息包 Header
func Format(msg []byte, cmd uint16) bool {
	size := len(msg)
	if size < 4 || size > math.MaxUint16+4 {
		return false
	}
	binary.LittleEndian.PutUint16(msg, uint16(len(msg)-4))
	binary.LittleEndian.PutUint16(msg[2:], cmd)
	return true
}

// Len 返回 默認 消息包 長度
func Len(bodyLen int) int {
	if bodyLen < 1 {
		return 4
	}
	return bodyLen + 4
}

type _Analyst struct {
	header int
	body   AnalyzeFunc
}

func (a *_Analyst) Header() int {
	return a.header
}
func (a *_Analyst) Body(header []byte) (int, error) {
	return a.body(header)
}

type _DefaultAnalyst struct {
}

func (_DefaultAnalyst) Header() int {
	return 4
}
func (_DefaultAnalyst) Body(header []byte) (int, error) {
	size := binary.LittleEndian.Uint16(header)
	return int(size), nil
}

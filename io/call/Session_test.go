package call_test

import (
	"bytes"
	"context"
	"testing"
	"time"

	"gitlab.com/king011/king-go/io/call"
	"gitlab.com/king011/king-go/io/message"
)

func TestSession(t *testing.T) {
	rw := &bytes.Buffer{}

	c := call.NewSession(message.NewParser(message.NewDefaultAnalyst()), rw)
	for i := 0; i < 3; i++ {
		ctx := context.Background()
		if i == 1 {
			ctx1, cancel := context.WithCancel(ctx)
			defer cancel()
			ctx = ctx1
		} else if i == 2 {
			ctx1, cancel := context.WithTimeout(ctx, time.Second)
			defer cancel()
			ctx = ctx1
		}
		str := "cerberus is an idea"
		msg, e := message.Marshal(1, []byte(str))
		if e != nil {
			t.Fatal(e)
		}

		response, e := c.Call(ctx, msg)
		if e != nil {
			t.Fatal(e)
		}

		if !bytes.Equal(msg, response) {
			t.Fatal("request not equal response")
		}
	}

}

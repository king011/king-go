// Package call 定義了從 rw io.ReadWriter 中 進行 遠程 調用的 輔助功能
package call

import (
	"context"
	"io"

	"gitlab.com/king011/king-go/io/message"
)

// Session 一個 用於 rpc 的 Session
type Session struct {
	Parser message.Parser
	RW     io.ReadWriter
}

// NewSession 創建一個 用於 rpc 的 Session
func NewSession(parser message.Parser, rw io.ReadWriter) (c *Session) {
	return &Session{
		Parser: parser,
		RW:     rw,
	}
}

type _Response struct {
	Data  []byte
	Error error
}

// Call 進行 遠端 調用
//
// 向 遠端 發送 一個 request 包 並且 接收一個 response 響應包
func (c *Session) Call(ctx context.Context, request []byte) (response []byte, e error) {
	e = ctx.Err()
	if e != nil {
		return
	}

	done := ctx.Done()
	if done == nil {
		rw := c.RW
		// write
		_, e = rw.Write(request)
		if e != nil {
			return
		}

		// read
		response, e = c.Parser.ReadMessage(rw)
		if e != nil {
			return
		}
	} else {
		// 執行操作
		ch := make(chan _Response)
		go c.call(ch, request)

		// 等待 操作 完成
		select {
		case <-done:
			e = ctx.Err()
			go func() {
				<-ch
			}()
		case rs := <-ch:
			if rs.Error == nil {
				response = rs.Data
			} else {
				e = rs.Error
			}
		}
	}
	return
}
func (c *Session) call(ch chan _Response, request []byte) {
	rw := c.RW
	// write
	_, e := rw.Write(request)
	if e != nil {
		ch <- _Response{
			Error: e,
		}
		return
	}

	// read
	msg, e := c.Parser.ReadMessage(rw)
	if e != nil {
		ch <- _Response{
			Error: e,
		}
		return
	}

	ch <- _Response{
		Data: msg,
	}
}

// Send 向 遠端 發送 一個 消息包
func (c *Session) Send(ctx context.Context, msg []byte) (e error) {
	e = ctx.Err()
	if e != nil {
		return
	}

	done := ctx.Done()
	if done == nil {
		rw := c.RW
		// write
		_, e = rw.Write(msg)
		if e != nil {
			return
		}

	} else {
		// 執行操作
		ch := make(chan error)
		go c.send(ch, msg)

		// 等待 操作 完成
		select {
		case <-done:
			e = ctx.Err()
			go func() {
				<-ch
			}()
		case e = <-ch:
		}
	}
	return
}
func (c *Session) send(ch chan error, msg []byte) {
	rw := c.RW
	// write
	_, e := rw.Write(msg)
	if e != nil {
		ch <- e
		return
	}

	ch <- nil
}

// Recv 接收一個 消息包
func (c *Session) Recv(ctx context.Context) (msg []byte, e error) {
	e = ctx.Err()
	if e != nil {
		return
	}

	done := ctx.Done()
	if done == nil {
		rw := c.RW

		// read
		msg, e = c.Parser.ReadMessage(rw)
		if e != nil {
			return
		}
	} else {
		// 執行操作
		ch := make(chan _Response)
		go c.recv(ch)

		// 等待 操作 完成
		select {
		case <-done:
			e = ctx.Err()
			go func() {
				<-ch
			}()
		case rs := <-ch:
			if rs.Error == nil {
				msg = rs.Data
			} else {
				e = rs.Error
			}
		}
	}
	return
}
func (c *Session) recv(ch chan _Response) {
	rw := c.RW

	// read
	msg, e := c.Parser.ReadMessage(rw)
	if e != nil {
		ch <- _Response{
			Error: e,
		}
		return
	}

	ch <- _Response{
		Data: msg,
	}
}

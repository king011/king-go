package io

import (
	"io"
)

// BufferReader 帶 緩衝區 的 reader
type BufferReader interface {
	io.Reader
	// 返回 緩衝區
	ReaderBuffer() []byte
	// 返回 緩衝區中 已經緩存的 數據 不會讀出
	ReaderCache() []byte
	// 返回 當前 緩存 偏移
	ReaderOffset() int
}

// NewBufferReader 將 io.Reader 包裝成爲一個 帶緩衝區的 Reader
// 如果 len(buffer) == 0 則 不帶緩衝區 ReadCache/ReadBuffer 函數始終返回 nil
// BufferReader 不是 goroutine 安全的
func NewBufferReader(r io.Reader, buffer []byte) BufferReader {
	if len(buffer) == 0 {
		return _DirectReader{r}
	}
	return &_BufferReader{
		r:      r,
		buffer: buffer,
	}
}

// NewBufferReaderCached 類似 NewBufferReader 不過將 buffer[:cached] 作爲已緩存 數據
// 如果 cached > len(buffer) 將整個 buffer 作爲 已緩存 數據
func NewBufferReaderCached(r io.Reader, buffer []byte, cached int) BufferReader {
	if len(buffer) == 0 {
		return _DirectReader{r}
	}
	size := len(buffer)
	if cached > size {
		cached = size
	} else if cached < 0 {
		cached = 0
	}
	return &_BufferReader{
		r:      r,
		buffer: buffer,
		end:    cached,
	}
}

type _DirectReader struct {
	io.Reader
}

func (r _DirectReader) ReaderCache() []byte {
	return nil
}
func (r _DirectReader) ReaderBuffer() []byte {
	return nil
}
func (r _DirectReader) ReaderOffset() int {
	return 0
}

type _BufferReader struct {
	buffer []byte
	begin  int
	end    int
	r      io.Reader
}

func (r *_BufferReader) ReaderCache() []byte {
	if r.begin == r.end {
		return nil
	}
	return r.buffer[r.begin:r.end]
}
func (r *_BufferReader) ReaderBuffer() []byte {
	return r.buffer
}
func (r *_BufferReader) ReaderOffset() int {
	return r.begin
}
func (r *_BufferReader) Read(b []byte) (int, error) {
	if len(b) == 0 {
		return 0, nil
	}
	if r.begin == r.end {
		// 讀取數據到 緩存
		n, e := r.r.Read(r.buffer)
		if n != 0 {
			r.end = n
		}
		if e != nil {
			return 0, e
		}
	}
	buffer := r.buffer[r.begin:r.end]
	n := copy(b, buffer)
	r.begin += n
	if r.begin == r.end {
		r.begin = 0
		r.end = 0
	}
	return n, nil
}

func (r *_BufferReader) flush(w io.Writer) (sum int64, e error) {
	var n int
	for r.begin != r.end {
		n, e = w.Write(r.buffer[r.begin:r.end])
		if n != 0 {
			sum += int64(n)
			r.begin += n
			if r.begin == r.end {
				r.begin = 0
				r.end = 0
				break
			}
		}
		if e != nil {
			break
		}
	}
	return
}
func (r *_BufferReader) WriteTo(w io.Writer) (sum int64, e error) {
	// 清空 緩存
	sum, e = r.flush(w)
	if e != nil {
		return
	}
	// 調用原始 WriterTo
	if wt, ok := (r.r).(io.WriterTo); ok {
		var n int64
		n, e = wt.WriteTo(w)
		if n != 0 {
			sum += n
		}
		return
	}

	var n int
	buffer := r.buffer
	for {
		n, e = r.r.Read(buffer)
		if n != 0 {
			sum += int64(n)
			r.end = n
		}
		if e != nil {
			if e == io.EOF {
				e = nil
			}
			break
		}

		if r.begin != r.end {
			// 清空 緩存
			var fn int64
			fn, e = r.flush(w)
			if fn != 0 {
				sum += fn
			}
			if e != nil {
				break
			}
		}
	}

	return
}

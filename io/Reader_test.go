package io_test

import (
	"bytes"
	"io"
	"io/ioutil"
	"testing"

	kio "gitlab.com/king011/king-go/io"
)

func TestReader(t *testing.T) {
	str := "cerberus is an idea"
	for i := 0; i < len(str)+10; i++ {
		r := bytes.NewBufferString(str)
		b := make([]byte, i)
		br := kio.NewBufferReader(r, b)
		b, e := ioutil.ReadAll(br)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := br.ReaderOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := br.ReaderCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}

	for i := 0; i < len(str)+10; i++ {
		r := &_ReadBytes{
			r: bytes.NewBufferString(str),
		}
		b := make([]byte, i)
		br := kio.NewBufferReader(r, b)

		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		_, e := io.Copy(w, br)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := br.ReaderOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := br.ReaderCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
	for i := 0; i < len(str)+10; i++ {
		r := bytes.NewBufferString(str)
		b := make([]byte, i)
		br := kio.NewBufferReader(r, b)

		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		_, e := io.Copy(w, br)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := br.ReaderOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := br.ReaderCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
}
func TestReaderCached(t *testing.T) {
	str := "cerberus is an idea"
	for i := 2; i < len(str)+10; i++ {
		r := bytes.NewBufferString(str[2:])
		b := make([]byte, i)
		b[0] = 'c'
		b[1] = 'e'
		br := kio.NewBufferReaderCached(r, b, 2)
		b, e := ioutil.ReadAll(br)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := br.ReaderOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := br.ReaderCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
	for i := 2; i < len(str)+10; i++ {
		r := &_ReadBytes{
			r: bytes.NewBufferString(str[2:]),
		}
		b := make([]byte, i)
		b[0] = 'c'
		b[1] = 'e'
		br := kio.NewBufferReaderCached(r, b, 2)

		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		_, e := io.Copy(w, br)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := br.ReaderOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := br.ReaderCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
	for i := 2; i < len(str)+10; i++ {
		r := bytes.NewBufferString(str[2:])
		b := make([]byte, i)
		b[0] = 'c'
		b[1] = 'e'
		br := kio.NewBufferReaderCached(r, b, 2)

		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		_, e := io.Copy(w, br)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := br.ReaderOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := br.ReaderCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
}

type _WriteBytes struct {
	w *bytes.Buffer
}

func (w *_WriteBytes) Write(b []byte) (int, error) {
	return w.w.Write(b)
}

type _ReadBytes struct {
	r *bytes.Buffer
}

func (r *_ReadBytes) Read(b []byte) (int, error) {
	return r.r.Read(b)
}

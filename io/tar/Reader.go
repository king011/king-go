package tar

import (
	"archive/tar"
	"io"
)

// ReadFiles 將 tar 指定 檔案 數據 讀取 到 map 中 如果不存在 則 keys[name]=nil
func ReadFiles(r *tar.Reader, filename ...string) (keys map[string][]byte, e error) {
	keys = make(map[string][]byte)
	for _, name := range filename {
		keys[name] = nil
	}

	var header *tar.Header
	var ok bool
	var n int
	count := 0
	for count != len(keys) {
		header, e = r.Next()
		if e != nil {
			if e == io.EOF {
				e = nil
			}
			break
		}
		// 過濾掉 非檔案
		if header.Typeflag != tar.TypeReg && header.Typeflag != tar.TypeRegA {
			continue
		}

		// 過濾掉 不需要的 檔案
		if _, ok = keys[header.Name]; !ok {
			continue
		}

		// 解包
		b := make([]byte, header.Size)
		n, e = r.Read(b)
		if int64(n) == header.Size {
			keys[header.Name] = b
		} else if e != nil {
			break
		}
	}
	return
}

// ReadAll 將 tar 中所有檔案 數據 讀取 到 map 中
func ReadAll(r *tar.Reader) (keys map[string][]byte, e error) {
	keys = make(map[string][]byte)

	var header *tar.Header
	var n int
	for {
		header, e = r.Next()
		if e != nil {
			if e == io.EOF {
				e = nil
			}
			break
		}
		// 過濾掉 非檔案
		if header.Typeflag != tar.TypeReg && header.Typeflag != tar.TypeRegA {
			continue
		}

		// 解包
		b := make([]byte, header.Size)
		n, e = r.Read(b)
		if int64(n) == header.Size {
			keys[header.Name] = b
		} else if e != nil {
			break
		}
	}
	return
}

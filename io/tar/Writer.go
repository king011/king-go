package tar

import (
	"archive/tar"
	"errors"
	"gitlab.com/king011/king-go/os/fileperm"
	"io"
	"os"
)

// ErrLessWrite 寫入數據 比期望小
var errLessWrite = errors.New("less write")

// WrapperWriter 包裝了 archive/tar Writer
type WrapperWriter struct {
	*tar.Writer
}

// NewWriter .
func NewWriter(w io.Writer) *WrapperWriter {
	return &WrapperWriter{
		tar.NewWriter(w),
	}
}

// NewWrapperWriter .
func NewWrapperWriter(w *tar.Writer) *WrapperWriter {
	return &WrapperWriter{
		w,
	}
}

// AddSimpleFile 將 Reader 作爲一個 檔案 filename 使用默認 Header 添加到 tar 包
func (w *WrapperWriter) AddSimpleFile(filename string, r io.Reader, size int64) (n int, e error) {
	n, e = w.AddFile(
		&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     filename,
			Mode:     int64(fileperm.File),
			Uid:      os.Getuid(),
			Gid:      os.Getgid(),
			Size:     size,
		},
		r,
	)
	return
}

// AddSimpleFileByte 將 []byte 作爲一個 檔案 filename 使用默認 Header 添加到 tar 包
func (w *WrapperWriter) AddSimpleFileByte(filename string, b []byte) (n int, e error) {
	n, e = w.AddFileByte(
		&tar.Header{
			Typeflag: tar.TypeReg,
			Name:     filename,
			Mode:     int64(fileperm.File),
			Uid:      os.Getuid(),
			Gid:      os.Getgid(),
			Size:     int64(len(b)),
		},
		b,
	)
	return
}

// AddFile 將 Reader 作爲一個 檔案 filename 添加到 tar 包
func (w *WrapperWriter) AddFile(header *tar.Header, r io.Reader) (n int, e error) {
	// header
	if e = w.WriteHeader(header); e != nil {
		return
	}
	var written int64
	written, e = io.CopyN(w, r, header.Size)
	if written != 0 {
		n = int(written)
	}
	return
}

// AddFileByte 將 []byte 作爲一個 檔案 filename 添加到 tar 包
func (w *WrapperWriter) AddFileByte(header *tar.Header, b []byte) (n int, e error) {
	if int64(len(b)) < header.Size {
		e = errLessWrite
		return
	}
	b = b[:int(header.Size)]
	// header
	if e = w.WriteHeader(header); e != nil {
		return
	}

	// body
	n, e = w.Write(b)
	return
}

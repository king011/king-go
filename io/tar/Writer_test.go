package tar

import (
	"archive/tar"
	"bytes"
	"testing"
)

func TestWriterReader(t *testing.T) {
	s0 := []byte("this is test 0")
	s1 := []byte("this is 肏你媽的 測試 123")
	s2 := []byte("this is test 2")

	// create writer
	var buffer bytes.Buffer
	w := NewWriter(&buffer)

	// add file
	n, e := w.AddSimpleFileByte("s0.txt", s0)
	if e != nil {
		t.Fatal(e)
	} else if n != len(s0) {
		t.Fatal("bad write len")
	}
	n, e = w.AddSimpleFileByte("s1.txt", s1)
	if e != nil {
		t.Fatal(e)
	} else if n != len(s1) {
		t.Fatal("bad write len")
	}
	n, e = w.AddSimpleFileByte("s2.txt", s2)
	if e != nil {
		t.Fatal(e)
	} else if n != len(s2) {
		t.Fatal("bad write len")
	}

	// flush and close
	w.Close()

	// read
	r := tar.NewReader(&buffer)
	keys, e := ReadFiles(r, "s0.txt", "s1.txt", "s2.txt")
	if e != nil {
		t.Fatal(e)
	}
	if !bytes.Equal([]byte(s0), keys["s0.txt"]) {
		t.Fatal("r s0 not match")
	}
	if !bytes.Equal([]byte(s1), keys["s1.txt"]) {
		t.Fatal("r s1 not match")
	}
	if !bytes.Equal([]byte(s2), keys["s2.txt"]) {
		t.Fatal("r s2 not match")
	}
}

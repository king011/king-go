package io

import (
	"fmt"
	"io"
	"sync"
)

type safeWriter struct {
	sync.Mutex
	Writer io.Writer
}

// NewSafeWriter .
// Deprecated:
func NewSafeWriter(w io.Writer) io.Writer {
	return &safeWriter{
		Writer: w,
	}
}
func (w *safeWriter) Write(b []byte) (n int, e error) {
	w.Lock()
	n, e = w.Writer.Write(b)
	w.Unlock()
	return
}
func (w *safeWriter) ReadFrom(src io.Reader) (n int64, e error) {
	w.Lock()
	n, e = io.Copy(w.Writer, src)
	w.Unlock()
	return
}

// WriteAll .
// Deprecated:
func WriteAll(w io.Writer, b []byte) (e error) {
	if len(b) == 0 {
		return
	}

	var n, pos int
	for pos != len(b) {
		n, e = w.Write(b[pos:])
		if n > 0 {
			pos += n
		}
		if e != nil && e != io.ErrShortWrite {
			break
		}
	}
	return
}

// ReadAll .
// Deprecated:
func ReadAll(r io.Reader, b []byte) (e error) {
	if len(b) == 0 {
		return
	}

	var n, pos int
	for pos != len(b) {
		n, e = r.Read(b[pos:])
		if n > 0 {
			pos += n
		}
		if e != nil {
			break
		}
	}
	return
}

// ReadAllEx .
// Deprecated:
func ReadAllEx(r io.Reader, b []byte) (pos int, e error) {
	if len(b) == 0 {
		return
	}

	var n int
	for pos != len(b) {
		n, e = r.Read(b[pos:])
		if n > 0 {
			pos += n
		}
		if e != nil {
			break
		}
	}
	return
}

// WriteAllEx .
// Deprecated:
func WriteAllEx(w io.Writer, b []byte) (pos int, e error) {
	if len(b) == 0 {
		return
	}

	var n int
	for pos != len(b) {
		n, e = w.Write(b[pos:])
		fmt.Println(n, e)
		if n > 0 {
			pos += n
		}
		if e != nil && e != io.ErrShortWrite {
			break
		}
	}
	return
}

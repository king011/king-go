package io_test

import (
	"bytes"
	"io"
	"testing"

	kio "gitlab.com/king011/king-go/io"
)

func TestWriter(t *testing.T) {
	str := "cerberus is an idea"
	for i := 0; i < len(str)+10; i++ {
		r := &_ReadBytes{
			r: bytes.NewBufferString(str),
		}
		b := make([]byte, i)
		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		bw := kio.NewBufferWriter(w, b)
		_, e := io.Copy(kio.Writer(bw), r)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		_, e = bw.WriterFlush()
		if e != nil {
			t.Fatalf("buffer[%v] flush %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := bw.WriterOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := bw.WriterCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
	for i := 0; i < len(str)+10; i++ {
		r := &_ReadBytes{
			r: bytes.NewBufferString(str),
		}
		b := make([]byte, i)
		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		bw := kio.NewBufferWriter(w, b)
		_, e := io.Copy(bw, r)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		_, e = bw.WriterFlush()
		if e != nil {
			t.Fatalf("buffer[%v] flush %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := bw.WriterOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := bw.WriterCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
}
func TestWriterCached(t *testing.T) {
	str := "cerberus is an idea"
	for i := 2; i < len(str)+10; i++ {
		r := &_ReadBytes{
			r: bytes.NewBufferString(str[2:]),
		}
		b := make([]byte, i)
		b[0] = 'c'
		b[1] = 'e'
		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		bw := kio.NewBufferWriterCached(w, b, 2)
		_, e := io.Copy(kio.Writer(bw), r)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		_, e = bw.WriterFlush()
		if e != nil {
			t.Fatalf("buffer[%v] flush %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := bw.WriterOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := bw.WriterCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
	for i := 2; i < len(str)+10; i++ {
		r := &_ReadBytes{
			r: bytes.NewBufferString(str[2:]),
		}
		b := make([]byte, i)
		b[0] = 'c'
		b[1] = 'e'
		w := &_WriteBytes{
			w: &bytes.Buffer{},
		}
		bw := kio.NewBufferWriterCached(w, b, 2)
		_, e := io.Copy(bw, r)
		if e != nil {
			t.Fatalf("buffer[%v] %v", i, e)
		}
		_, e = bw.WriterFlush()
		if e != nil {
			t.Fatalf("buffer[%v] flush %v", i, e)
		}
		b = w.w.Bytes()
		dst := string(b)
		if str != dst {
			t.Fatalf("buffer[%v] %v != %v %v", i, str, dst, b)
		}
		offset := bw.WriterOffset()
		if offset != 0 {
			t.Fatalf("buffer[%v] offset == %v", i, offset)
		}
		cache := bw.WriterCache()
		if cache != nil {
			t.Fatalf("buffer[%v] cache != nil", i)
		}
	}
}

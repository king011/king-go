package io

import (
	"io"
)

// BufferWriter 帶 緩衝區 的 reader
type BufferWriter interface {
	io.Writer
	// 返回 緩衝區
	WriterBuffer() []byte
	// 返回 緩衝區中 已經緩存的 數據
	WriterCache() []byte
	// 返回 當前 緩存 偏移
	WriterOffset() int
	// 刷新 寫入緩存
	// 對於 io.WriteCloser 在 Close前 應該要 調用 WriterFlush 將緩衝區內容 寫入到 io.Writer 中
	WriterFlush() (int, error)
}

// NewBufferWriter 將 io.Writer 包裝成爲一個 帶緩衝區的 Writer
// 如果 len(buffer) == 0 則 不帶緩衝區 WriteCache/WriteBuffer 函數始終返回 nil
// BufferWriter 不是 goroutine 安全的
func NewBufferWriter(w io.Writer, buffer []byte) BufferWriter {
	if len(buffer) == 0 {
		return _DirectWriter{w}
	}
	return &_BufferWriter{
		w:      w,
		buffer: buffer,
	}
}

// NewBufferWriterCached 類似 NewBufferWriter 不過將 buffer[:cached] 作爲已緩存 數據
// 如果 cached > len(buffer) 將整個 buffer 作爲 已緩存 數據
func NewBufferWriterCached(w io.Writer, buffer []byte, cached int) BufferWriter {
	if len(buffer) == 0 {
		return _DirectWriter{w}
	}
	size := len(buffer)
	if cached > size {
		cached = size
	} else if cached < 0 {
		cached = 0
	}
	return &_BufferWriter{
		w:      w,
		buffer: buffer,
		end:    cached,
	}
}

type _DirectWriter struct {
	io.Writer
}

func (w _DirectWriter) WriterCache() []byte {
	return nil
}
func (w _DirectWriter) WriterBuffer() []byte {
	return nil
}
func (w _DirectWriter) WriterOffset() int {
	return 0
}
func (w _DirectWriter) WriterFlush() (int, error) {
	return 0, nil
}

type _BufferWriter struct {
	buffer []byte
	begin  int
	end    int
	w      io.Writer
}

func (w *_BufferWriter) WriterCache() []byte {
	if w.begin == w.end {
		return nil
	}
	return w.buffer[w.begin:w.end]
}
func (w *_BufferWriter) WriterBuffer() []byte {
	return w.buffer
}
func (w *_BufferWriter) WriterOffset() int {
	return w.begin
}
func (w *_BufferWriter) WriterFlush() (sum int, e error) {
	var n int
	for w.begin != w.end {
		buffer := w.buffer[w.begin:w.end]
		n, e = w.w.Write(buffer)
		if n != 0 {
			w.begin += n
			if w.begin == w.end {
				w.begin = 0
				w.end = 0
				break
			}
		}
		if e != nil {
			break
		}
	}
	return
}
func (w *_BufferWriter) Write(b []byte) (sum int, e error) {
	// 緩衝區 已經滿 刷新緩衝區
	if w.end == len(w.buffer) {
		_, e = w.WriterFlush()
		if e != nil {
			return
		}
	}

	for len(b) != 0 {
		n := w.writeToBuffer(b)
		if n != 0 {
			sum += n
			w.end += n
			b = b[n:]
		}

		// 緩衝區 已經滿 刷新緩衝區
		if w.end == len(w.buffer) {
			_, e = w.WriterFlush()
			if e != nil {
				break
			}
		}
	}
	return
}
func (w *_BufferWriter) writeToBuffer(b []byte) (n int) {
	// 緩衝區 已滿 直接返回
	if w.end == len(w.buffer) {
		return
	}
	n = copy(w.buffer[w.end:], b)
	return
}

func (w *_BufferWriter) ReadFrom(r io.Reader) (sum int64, e error) {
	// 清空 緩存
	var n int
	n, e = w.WriterFlush()
	if n != 0 {
		sum += int64(n)
	}
	if e != nil {
		return
	}

	// 調用原始 ReadFrom
	if rf, ok := (w.w).(io.ReaderFrom); ok {
		var n int64
		n, e = rf.ReadFrom(r)
		if n != 0 {
			sum += n
		}
		return
	}

	buffer := w.buffer
	for {
		n, e = r.Read(buffer)
		if n != 0 {
			sum += int64(n)
			w.end = n
		}
		if e != nil {
			if e == io.EOF {
				e = nil
			}
			break
		}

		if w.begin != w.end {
			// 清空 緩存
			n, e = w.WriterFlush()
			if n != 0 {
				sum += int64(n)
			}
			if e != nil {
				break
			}
		}
	}
	return
}

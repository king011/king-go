package io

import (
	"context"
	"io"
)

// CopyBuffer 類似 io.copyBuffer
// 不過 不會調用 WriteTo/ReaderFrom 所以可以放心的在 WriteTo/ReaderFrom的實現中 調用此函數
func CopyBuffer(dst io.Writer, src io.Reader, buf []byte) (written int64, err error) {
	size := 32 * 1024
	if l, ok := src.(*io.LimitedReader); ok && int64(size) > l.N {
		if l.N < 1 {
			size = 1
		} else {
			size = int(l.N)
		}
	}
	if buf == nil || len(buf) == 0 {
		buf = make([]byte, size)
	}
	for {
		nr, er := src.Read(buf)
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}
		if er != nil {
			if er != io.EOF {
				err = er
			}
			break
		}
	}
	return written, err
}

type _Result struct {
	E error
	N int
}

// WriteContext write with Context
func WriteContext(ctx context.Context, w io.Writer, b []byte) (n int, e error) {
	if len(b) == 0 {
		return
	}

	e = ctx.Err()
	if e != nil {
		return
	}
	done := ctx.Done()
	if done == nil {
		// write
		n, e = w.Write(b)
	} else {
		// 執行操作
		ch := make(chan _Result)
		go writeContext(ch, w, b)

		// 等待 操作 完成
		select {
		case <-done:
			e = ctx.Err()
			go func() {
				<-ch
			}()
		case rs := <-ch:
			n = rs.N
			e = rs.E
		}
	}
	return
}
func writeContext(ch chan _Result, w io.Writer, b []byte) {
	n, e := w.Write(b)
	if e != nil {
		ch <- _Result{
			E: e,
			N: n,
		}
		return
	}
	ch <- _Result{
		N: n,
	}
}

// ReadContext read with Context
func ReadContext(ctx context.Context, r io.Reader, b []byte) (n int, e error) {
	if len(b) == 0 {
		return
	}
	e = ctx.Err()
	if e != nil {
		return
	}

	done := ctx.Done()
	if done == nil {
		// read
		n, e = r.Read(b)
	} else {
		// 執行操作
		ch := make(chan _Result)
		go readContext(ch, r, b)

		// 等待 操作 完成
		select {
		case <-done:
			e = ctx.Err()
			go func() {
				<-ch
			}()
		case rs := <-ch:
			n = rs.N
			e = rs.E
		}
	}
	return
}
func readContext(ch chan _Result, r io.Reader, b []byte) {
	n, e := r.Read(b)
	if e != nil {
		ch <- _Result{
			E: e,
			N: n,
		}
		return
	}
	ch <- _Result{
		N: n,
	}
}

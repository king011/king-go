# memory
memory 提供了 對象池 提升對象 創建速度

# Allocator

Allocator 接口 定義了 內存分配器的 解包接口

```go
// Allocator 手動的 內存管理器
type Allocator interface {
	// 從內存池中 分配一個 對象 如果內存池 爲空 從os 分配
	Get() (obj interface{})

	// 從內存池中 分配一個 對象 如果內存池 爲空 返回 nil
	GetFromPool() (obj interface{})

	// 將 對象 投入 內存池 中
	//
	// 返回 是否 投入 成功
	//
	// 如果 成功 且設置了 DestroyF 函數 則會爲 obj 調用 DestroyF 函數
	Put(obj interface{}) (ok bool)

	// 清空 緩存的 對象
	Clear()
	// 返回 當前 緩存對象 數量
	Len() (n int)
	// 返回 當前 能夠容納的 緩存數量
	Cap() (n int)
}
```

# impl

* Chunk 一個固定塊的 內存
* PoolStatic/SafePoolStatic 將 多個 Chunk 組合起來的 對象池
* PoolDynamic/SafePoolDynamic 將 多個 Chunk 組合起來的 對象池 會自動 調整 容量
package memory

// CreateF 對象創建 函數
type CreateF func() (obj interface{})

// DestroyF 對象銷毀 函數
type DestroyF func(obj interface{})

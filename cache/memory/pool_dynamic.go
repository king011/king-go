package memory

import "sync"

// PoolDynamic 一個 自動調整容量的 內存池
type PoolDynamic struct {
	chunk []Chunk
	// 使用 chunk 數量
	n int

	chunkSize int

	createF  CreateF
	destroyF DestroyF

	// 有效 chunk
	effectiveChunk int
	call           int64
}

// NewPoolDynamic 創建一個 PoolDynamic (非 goroutine 安全)
//
// createF 定義了 如何 創建對象
//
// destroyF 定義了 如何 銷毀 對象 如果爲 nil 則不執行 額外銷毀操作
//
// chunkSize 定義每個 Chunk 大小 最小爲 10
//
// chunks 定義了 Chunk 初始化 數量 最小爲 1
func NewPoolDynamic(createF CreateF,
	destroyF DestroyF,
	chunkSize int,
	chunks int,
) *PoolDynamic {
	var pool PoolDynamic
	pool.init(createF, destroyF, chunkSize, chunks)
	return &pool
}
func (pool *PoolDynamic) init(createF CreateF,
	destroyF DestroyF,
	chunkSize int,
	chunks int,
) {
	if pool.chunk != nil {
		panic("PoolDynamic already init")
	}
	if chunkSize < 10 {
		chunkSize = 10
	}
	if chunks < 1 {
		chunks = 1
	}
	chunk := make([]Chunk, chunks)
	for i := 0; i < chunks; i++ {
		chunk[i].init(createF, destroyF, chunkSize)
	}
	pool.chunk = chunk
	pool.chunkSize = chunkSize
	pool.createF = createF
	pool.destroyF = destroyF
	pool.effectiveChunk = -1
}

// Get 實現 Allocator.Get 接口
func (pool *PoolDynamic) Get() (obj interface{}) {
	if pool.n > 0 {
		i := pool.n - 1
		obj = pool.chunk[i].Get()
		if pool.chunk[i].Len() == 0 {
			pool.n = i
		}
		// 調整 過期 chunk
		pool.adjust(i)
	} else {
		obj = pool.createF()
	}
	return
}

// GetFromPool 實現 Allocator.GetFromPool 接口
func (pool *PoolDynamic) GetFromPool() (obj interface{}) {
	if pool.n > 0 {
		i := pool.n - 1
		obj = pool.chunk[i].Get()
		if pool.chunk[i].Len() == 0 {
			pool.n = i
		}
		// 調整 過期 chunk
		pool.adjust(i)
	}
	return
}

// Put 實現 Allocator.Put 接口
func (pool *PoolDynamic) Put(obj interface{}) (ok bool) {
	if pool.n == 0 {
		pool.n = 1
		pool.chunk[0].Put(obj)
	} else {
		if !pool.chunk[pool.n-1].Put(obj) { // chunk 已滿 Put 失敗
			if pool.n == len(pool.chunk) { // 增加 新 chunk
				pool.chunk = append(pool.chunk, Chunk{})
				pool.chunk[pool.n].init(pool.createF, pool.destroyF, pool.chunkSize)
			}
			// 移動到下個 chunk
			pool.chunk[pool.n].Put(obj)
			pool.n++
		}
	}
	ok = true
	return
}

// Clear 實現 Allocator.Clear 接口
func (pool *PoolDynamic) Clear() {
	if pool.n == 0 {
		return
	}
	for i := 0; i < pool.n; i++ {
		pool.chunk[i].Clear()
	}
	pool.n = 0
	// 調整 過期 chunk
	pool.effectiveChunk = -1
}

// Len 實現 Allocator.Len 接口
func (pool *PoolDynamic) Len() (n int) {
	if pool.n == 1 {
		n = pool.chunk[0].n
	} else if pool.n > 1 {
		n = pool.chunkSize*(pool.n-1) + pool.chunk[0].n
	}
	return
}

// Cap 實現 Allocator.Cap 接口
func (pool *PoolDynamic) Cap() (n int) {
	return pool.chunkSize * len(pool.chunk)
}
func (pool *PoolDynamic) adjust(i int) {
	if pool.effectiveChunk == -1 {
		pool.effectiveChunk = i
		pool.call = 0
		return
	}
	// 更新 有效 chunk
	if i < pool.effectiveChunk {
		i = pool.effectiveChunk
		pool.call++
	} else {
		// 判斷 過期
		if pool.call > 1000 {
			// 刪除 chunk
			pool.resetChunk(i)
			pool.effectiveChunk = -1
		}
	}
}
func (pool *PoolDynamic) resetChunk(n int) {
	for ; pool.n > 1 && n > 0; n-- {
		i := pool.n - 1
		pool.chunk[i].Clear()
		pool.n = i
	}
}

// SafePoolDynamic 一個 自動調整容量的 內存池
type SafePoolDynamic struct {
	mutex sync.Mutex
	impl  PoolStatic
}

// NewSafePoolDynamic 創建一個 PoolDynamic (goroutine 安全)
//
// createF 定義了 如何 創建對象
//
// destroyF 定義了 如何 銷毀 對象 如果爲 nil 則不執行 額外銷毀操作
//
// chunkSize 定義每個 Chunk 大小 最小爲 10
//
// chunks 定義了 Chunk 初始化 數量 最小爲 1
func NewSafePoolDynamic(createF CreateF,
	destroyF DestroyF,
	chunkSize int,
	chunks int,
) *SafePoolDynamic {
	var pool SafePoolDynamic
	pool.impl.init(createF, destroyF, chunkSize, chunks)
	return &pool
}

// Get 實現 Allocator.Get 接口
func (pool *SafePoolDynamic) Get() (obj interface{}) {
	pool.mutex.Lock()
	obj = pool.impl.Get()
	pool.mutex.Unlock()
	return
}

// GetFromPool 實現 Allocator.GetFromPool 接口
func (pool *SafePoolDynamic) GetFromPool() (obj interface{}) {
	pool.mutex.Lock()
	obj = pool.impl.GetFromPool()
	pool.mutex.Unlock()
	return
}

// Put 實現 Allocator.Put 接口
func (pool *SafePoolDynamic) Put(obj interface{}) (ok bool) {
	pool.mutex.Lock()
	ok = pool.impl.Put(obj)
	pool.mutex.Unlock()
	return
}

// Clear 實現 Allocator.Clear 接口
func (pool *SafePoolDynamic) Clear() {
	pool.mutex.Lock()
	pool.impl.Clear()
	pool.mutex.Unlock()
}

// Len 實現 Allocator.Len 接口
func (pool *SafePoolDynamic) Len() (n int) {
	pool.mutex.Lock()
	n = pool.impl.Len()
	pool.mutex.Unlock()
	return
}

// Cap 實現 Allocator.Cap 接口
func (pool *SafePoolDynamic) Cap() (n int) {
	pool.mutex.Lock()
	n = pool.impl.Cap()
	pool.mutex.Unlock()
	return
}

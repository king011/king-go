package memory

import "sync"

// PoolStatic 一個 固定容量的 內存池
type PoolStatic struct {
	chunk []Chunk
	// 使用 chunk 數量
	n int

	chunkSize int

	createF CreateF
}

// NewPoolStatic 創建一個 PoolStatic (非 goroutine 安全)
//
// createF 定義了 如何 創建對象
//
// destroyF 定義了 如何 銷毀 對象 如果爲 nil 則不執行 額外銷毀操作
//
// chunkSize 定義每個 Chunk 大小 最小爲 10
//
// chunks 定義了 Chunk 數量 最小爲 2
//
// PoolStatic 的上限爲 chunkSize * chunks 當 達到此值時 Put 將直接 返回 false
func NewPoolStatic(createF CreateF,
	destroyF DestroyF,
	chunkSize int,
	chunks int,
) *PoolStatic {
	var pool PoolStatic
	pool.init(createF, destroyF, chunkSize, chunks)
	return &pool
}
func (pool *PoolStatic) init(createF CreateF,
	destroyF DestroyF,
	chunkSize int,
	chunks int,
) {
	if pool.chunk != nil {
		panic("PoolStatic already init")
	}
	if chunkSize < 10 {
		chunkSize = 10
	}
	if chunks < 2 {
		chunks = 2
	}
	chunk := make([]Chunk, chunks)
	for i := 0; i < chunks; i++ {
		chunk[i].init(createF, destroyF, chunkSize)
	}
	pool.chunk = chunk
	pool.chunkSize = chunkSize
	pool.createF = createF
}

// Get 實現 Allocator.Get 接口
func (pool *PoolStatic) Get() (obj interface{}) {
	if pool.n > 0 {
		i := pool.n - 1
		obj = pool.chunk[i].Get()
		if pool.chunk[i].Len() == 0 {
			pool.n = i
		}
	} else {
		obj = pool.createF()
	}
	return
}

// GetFromPool 實現 Allocator.GetFromPool 接口
func (pool *PoolStatic) GetFromPool() (obj interface{}) {
	if pool.n > 0 {
		i := pool.n - 1
		obj = pool.chunk[i].Get()
		if pool.chunk[i].Len() == 0 {
			pool.n = i
		}
	}
	return
}

// Put 實現 Allocator.Put 接口
func (pool *PoolStatic) Put(obj interface{}) (ok bool) {
	if pool.n == 0 {
		pool.n = 1
		pool.chunk[0].Put(obj)
		ok = true
	} else {
		if !pool.chunk[pool.n-1].Put(obj) { // chunk 已滿 Put 失敗
			if pool.n != len(pool.chunk) { // 還有可用 chunk
				// 移動到下個 chunk
				pool.chunk[pool.n].Put(obj)
				pool.n++
				ok = true
			}
		}
	}
	return
}

// Clear 實現 Allocator.Clear 接口
func (pool *PoolStatic) Clear() {
	if pool.n == 0 {
		return
	}
	for i := 0; i < pool.n; i++ {
		pool.chunk[i].Clear()
	}
	pool.n = 0
}

// Len 實現 Allocator.Len 接口
func (pool *PoolStatic) Len() (n int) {
	if pool.n == 1 {
		n = pool.chunk[0].n
	} else if pool.n > 1 {
		n = pool.chunkSize*(pool.n-1) + pool.chunk[0].n
	}
	return
}

// Cap 實現 Allocator.Cap 接口
func (pool *PoolStatic) Cap() (n int) {
	return pool.chunkSize * len(pool.chunk)
}

// SafePoolStatic PoolStatic 的 goroutine safe 版本
type SafePoolStatic struct {
	mutex sync.Mutex
	impl  PoolStatic
}

// NewSafePoolStatic 創建一個 PoolStatic (goroutine 安全)
//
// createF 定義了 如何 創建對象
//
// destroyF 定義了 如何 銷毀 對象 如果爲 nil 則不執行 額外銷毀操作
//
// chunkSize 定義每個 Chunk 大小 最小爲 10
//
// chunks 定義了 Chunk 數量 最小爲 2
//
// PoolStatic 的上限爲 chunkSize * chunks 當 達到此值時 Put 將直接 返回 false
func NewSafePoolStatic(createF CreateF,
	destroyF DestroyF,
	chunkSize int,
	chunks int,
) *SafePoolStatic {
	var pool SafePoolStatic
	pool.impl.init(createF, destroyF, chunkSize, chunks)
	return &pool
}

// Get 實現 Allocator.Get 接口
func (pool *SafePoolStatic) Get() (obj interface{}) {
	pool.mutex.Lock()
	obj = pool.impl.Get()
	pool.mutex.Unlock()
	return
}

// GetFromPool 實現 Allocator.GetFromPool 接口
func (pool *SafePoolStatic) GetFromPool() (obj interface{}) {
	pool.mutex.Lock()
	obj = pool.impl.GetFromPool()
	pool.mutex.Unlock()
	return
}

// Put 實現 Allocator.Put 接口
func (pool *SafePoolStatic) Put(obj interface{}) (ok bool) {
	pool.mutex.Lock()
	ok = pool.impl.Put(obj)
	pool.mutex.Unlock()
	return
}

// Clear 實現 Allocator.Clear 接口
func (pool *SafePoolStatic) Clear() {
	pool.mutex.Lock()
	pool.impl.Clear()
	pool.mutex.Unlock()
}

// Len 實現 Allocator.Len 接口
func (pool *SafePoolStatic) Len() (n int) {
	pool.mutex.Lock()
	n = pool.impl.Len()
	pool.mutex.Unlock()
	return
}

// Cap 實現 Allocator.Cap 接口
func (pool *SafePoolStatic) Cap() (n int) {
	pool.mutex.Lock()
	n = pool.impl.Cap()
	pool.mutex.Unlock()
	return
}

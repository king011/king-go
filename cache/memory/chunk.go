package memory

import (
	"sync"
)

// Chunk 分塊內存池的 一個 小塊
type Chunk struct {
	createF  CreateF
	destroyF DestroyF
	data     []interface{}
	// 使用緩存 data 數量
	n int
}

// NewChunk 創建一個 內存塊 (非 goroutine 安全)
//
// createF 定義了 如何 創建對象
//
// destroyF 定義了 如何 銷毀 對象 如果爲 nil 則不執行 額外銷毀操作
//
// n 緩存對象大小 最小爲 10
func NewChunk(createF CreateF,
	destroyF DestroyF,
	n int,
) *Chunk {
	chunk := &Chunk{}
	chunk.init(createF, destroyF, n)
	return chunk
}
func (chunk *Chunk) init(createF CreateF,
	destroyF DestroyF,
	n int,
) {
	if chunk.data != nil {
		panic("Chunk already init")
	}
	if n < 10 {
		n = 10
	}
	chunk.createF = createF
	chunk.destroyF = destroyF
	chunk.data = make([]interface{}, n)
}

// Get 實現 Allocator.Get 接口
func (chunk *Chunk) Get() (obj interface{}) {
	if chunk.n > 0 {
		chunk.n--
		obj = chunk.data[chunk.n]
		chunk.data[chunk.n] = nil
	} else {
		obj = chunk.createF()
	}
	return
}

// GetFromPool 實現 Allocator.GetFromPool 接口
func (chunk *Chunk) GetFromPool() (obj interface{}) {
	if chunk.n > 0 {
		chunk.n--
		obj = chunk.data[chunk.n]
		chunk.data[chunk.n] = nil
	}
	return
}

// Put 實現 Allocator.Put 接口
func (chunk *Chunk) Put(obj interface{}) (ok bool) {
	if chunk.n < len(chunk.data) {
		if chunk.destroyF != nil {
			chunk.destroyF(obj)
		}
		chunk.data[chunk.n] = obj
		chunk.n++
		ok = true
	}
	// else 緩存已滿 直接 返回
	return
}

// Clear 實現 Allocator.Clear 接口
func (chunk *Chunk) Clear() {
	if chunk.n == 0 {
		return
	}
	for i := 0; i < chunk.n; i++ {
		chunk.data[i] = nil
	}
	chunk.n = 0
}

// Len 實現 Allocator.Len 接口
func (chunk *Chunk) Len() (n int) {
	return chunk.n
}

// Cap 實現 Allocator.Cap 接口
func (chunk *Chunk) Cap() (n int) {
	return len(chunk.data)
}

// SafeChunk Chunk 的 goroutine safe 版本
type SafeChunk struct {
	mutex sync.Mutex
	impl  Chunk
}

// NewSafeChunk 創建一個 內存塊 (非 goroutine 安全)
//
// createF 定義了 如何 創建對象
//
// destroyF 定義了 如何 銷毀 對象 如果爲 nil 則不執行 額外銷毀操作
//
// n 緩存對象大小 最小爲 10
func NewSafeChunk(createF CreateF,
	destroyF DestroyF,
	n int,
) (chunk *SafeChunk) {
	chunk = &SafeChunk{}
	chunk.impl.init(createF, destroyF, n)
	return
}

// Get 實現 Allocator.Get 接口
func (chunk *SafeChunk) Get() (obj interface{}) {
	chunk.mutex.Lock()
	obj = chunk.impl.Get()
	chunk.mutex.Unlock()
	return
}

// GetFromPool 實現 Allocator.GetFromPool 接口
func (chunk *SafeChunk) GetFromPool() (obj interface{}) {
	chunk.mutex.Lock()
	obj = chunk.impl.GetFromPool()
	chunk.mutex.Unlock()
	return
}

// Put 實現 Allocator.Put 接口
func (chunk *SafeChunk) Put(obj interface{}) (ok bool) {
	chunk.mutex.Lock()
	ok = chunk.impl.Put(obj)
	chunk.mutex.Unlock()
	return
}

// Clear 實現 Allocator.Clear 接口
func (chunk *SafeChunk) Clear() {
	chunk.mutex.Lock()
	chunk.impl.Clear()
	chunk.mutex.Unlock()
}

// Len 實現 Allocator.Len 接口
func (chunk *SafeChunk) Len() (n int) {
	chunk.mutex.Lock()
	n = chunk.impl.Len()
	chunk.mutex.Unlock()
	return
}

// Cap 實現 Allocator.Cap 接口
func (chunk *SafeChunk) Cap() (n int) {
	chunk.mutex.Lock()
	n = chunk.impl.Cap()
	chunk.mutex.Unlock()
	return
}

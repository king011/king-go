package lru

import (
	"fmt"
	"sync"
	"testing"
)

type _Element struct {
	Next *_Element
	Pre  *_Element

	Key   IKey
	Value IValue
}
type lruImpl struct {
	RW *sync.RWMutex

	Max int

	//緩存 的節點
	Keys  map[IKey]*_Element
	Front *_Element
	Back  *_Element
}

func newLRUImpl(maxElementSize int, safe bool) *lruImpl {
	if maxElementSize < 1 {
		maxElementSize = 1
	}
	return &lruImpl{
		Max: maxElementSize,

		Keys: make(map[IKey]*_Element),
	}
}

func (l *lruImpl) Len() (n int) {
	if l.RW == nil {
		n = len(l.Keys)
	} else {
		l.RW.RLock()
		n = len(l.Keys)
		l.RW.RUnlock()
	}
	return
}

func (l *lruImpl) Cap() (n int) {
	if l.RW == nil {
		n = l.Max
	} else {
		l.RW.RLock()
		n = l.Max
		l.RW.RUnlock()
	}
	return
}
func (l *lruImpl) Clear() {
	if l.RW == nil {
		l.unsafeClear()
	} else {
		l.RW.Lock()
		l.unsafeClear()
		l.RW.Unlock()
	}
}
func (l *lruImpl) unsafeClear() {
	for key := range l.Keys {
		//刪除 map
		delete(l.Keys, key)
	}
	l.Front = nil
	l.Back = nil
}

func (l *lruImpl) Delete(key IKey) {
	if l.RW == nil {
		l.unsafeDelete(key)
	} else {
		l.RW.Lock()
		l.unsafeDelete(key)
		l.RW.Unlock()
	}
}
func (l *lruImpl) unsafeDelete(key IKey) {
	ele, ok := l.Keys[key]
	// 緩存 不存在 直接 返回
	if !ok {
		return
	}

	// 刪除 map
	delete(l.Keys, key)

	// 刪除 鏈表
	l.unsafeRemoveList(ele)
}
func (l *lruImpl) unsafeRemoveList(ele *_Element) {
	if ele.Next == nil {
		l.Back = ele.Pre
	} else { //需要 設置 next
		ele.Next.Pre = ele.Pre
		if ele.Pre == nil {
			l.Front = ele.Next
		} else {
			ele.Pre.Next = ele.Next
		}
	}
	if ele.Pre == nil {
		l.Front = ele.Next
	} else { //需要 設置 pre
		ele.Pre.Next = ele.Next
		if ele.Next == nil {
			l.Back = ele.Pre
		} else {
			ele.Next.Pre = ele.Pre
		}
	}
}

func (l *lruImpl) Ok(key IKey) (ok bool) {
	if l.RW == nil {
		_, ok = l.Keys[key]
	} else {
		l.RW.RLock()
		_, ok = l.Keys[key]
		l.RW.RUnlock()
	}
	return
}

func (l *lruImpl) Get(key IKey) IValue {
	if l.RW != nil {
		l.RW.RLock()
		defer l.RW.RUnlock()
	}

	ele, ok := l.Keys[key]
	//緩存 不存在 直接 返回
	if !ok {
		return nil
	}

	//移動到 Back
	l.unsafeToBack(ele)

	return ele.Value
}
func (l *lruImpl) unsafeToBack(ele *_Element) {
	if ele.Next == nil {
		//本來就是 back 節點 直接返回
		return
	}

	ele.Next.Pre = ele.Pre
	if ele.Pre == nil {
		l.Front = ele.Next
	} else {
		ele.Pre.Next = ele.Next
	}

	ele.Next = nil
	ele.Pre = l.Back
	l.Back.Next = ele
	l.Back = ele
}

func (l *lruImpl) Set(key IKey, val IValue) {
	if l.RW != nil {
		l.RW.Lock()
		defer l.RW.Unlock()
	}

	//驗證 存在
	ele, ok := l.Keys[key]
	if ok {
		//更新
		ele.Value = val

		//移動到 Back
		l.unsafeToBack(ele)
	} else {
		//創建 新緩存
		if len(l.Keys) == l.Max &&
			l.Front != nil {

			//刪除 front
			delete(l.Keys, l.Front.Key)

			l.unsafeRemoveList(l.Front)
		}
		//創建
		l.unsafeNew(key, val)
	}
}

func (l *lruImpl) unsafeNew(key IKey, val IValue) {
	ele := &_Element{
		Pre:  l.Back,
		Next: nil,

		Key:   key,
		Value: val,
	}

	l.Keys[key] = ele
	if l.Back == nil {
		l.Front = ele
	} else {
		l.Back.Next = ele
	}
	l.Back = ele
}

func (l *lruImpl) Resize(percentage float64) int {
	if l.RW != nil {
		l.RW.Lock()
		defer l.RW.Unlock()
	}

	max := (int)((float64)(l.Max) * percentage)
	if max == l.Max {
		return len(l.Keys)
	} else if max == 0 {
		if len(l.Keys) != 0 {
			l.unsafeClear()
		}
		return 0
	} else if max == 1 {
		l.unsafeOnlyOne()
		return len(l.Keys)
	}

	for len(l.Keys) > max {
		l.unsafePopFront()
	}
	return len(l.Keys)
}
func (l *lruImpl) unsafeOnlyOne() {
	ele := l.Back
	if ele == nil { //沒有節點
		return
	} else if ele == l.Front { //只有一個節點
		return
	}

	ele.Pre = nil
	l.Front = ele
	for k := range l.Keys {
		if k != ele.Key {
			delete(l.Keys, k)
		}
	}
}
func (l *lruImpl) unsafePopFront() {
	ele := l.Front
	l.Front = ele.Next
	if ele.Next == nil {
		l.Back = nil
	} else {
		ele.Next.Pre = nil
	}
	delete(l.Keys, ele.Key)
}
func (l *lruImpl) debugPrint(t *testing.T) {
	node := l.Front
	if t == nil {
		fmt.Print("[")
	}
	sum := 0
	for node != nil {
		sum++
		if t == nil {
			fmt.Printf("%v=%v ,", node.Key, node.Value)
		}
		next := node.Next
		if next != nil {
			if next.Pre != node {
				t.Fatal("bad Pre")
			}
		}

		node = next
	}
	if t == nil {
		fmt.Print("]")
	}
	if len(l.Keys) != sum {
		t.Fatalf(" len(%v %v) ", len(l.Keys), sum)
	}
	if l.Back != nil {
		if t == nil {
			fmt.Printf(" %v=%v \n", l.Back.Key, l.Back.Value)
		}
	}
}

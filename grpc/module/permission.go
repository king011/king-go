package module

import "strings"

// Permission 權限輔助
type Permission struct {
	any map[string]bool
	val map[string]int64
}

// RegisterAny 註冊 Any 方法
func (p *Permission) RegisterAny(methods ...string) {
	if p.any == nil {
		p.any = make(map[string]bool)
	}
	for _, method := range methods {
		p.any[strings.ToLower(method)] = true
	}
}

// RegisterVal 註冊 需要 權限的 方法
func (p *Permission) RegisterVal(method string, val int64) {
	if p.val == nil {
		p.val = make(map[string]int64)
	}
	p.val[strings.ToLower(method)] = val
}

// IsAny 返回是否 Any 可訪問
func (p *Permission) IsAny(method string) bool {
	if p.any == nil {
		return false
	}
	return p.any[method]
}

// IsVal 返回是否 有權限訪問
func (p *Permission) IsVal(method string, vals []int64) bool {
	if p.val == nil {
		return true
	}
	if val, ok := p.val[method]; ok {
		for _, v := range vals {
			if v == val {
				return true
			}
		}
		return false
	}
	return true
}

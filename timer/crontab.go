package timer

import (
	"bytes"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

//類似 linux crontab 組件 的定時器
/*
#分鐘		小時		號數		月份		周星期
#[0,59]		[0,23]		[1,31]		[1~12]		[0,7] 0==7==星期天
0		13		*		*		0
*/
const (
	// 不限制 項目
	CrontabAny     = -1
	CrontabAnyName = "*"

	// 錯誤描述
	CrontabEmsgMinute = "minute must at range [0,59]"
	CrontabEmsgHour   = "hour must at range [0,23]"
	CrontabEmsgDay    = "day must at range [1,31]"
	CrontabEmsgMonth  = "month must at range [1,12]"
	CrontabEmsgWeek   = "week must at range [0,7]"
)

// CrontabCallback Crontab 定時器 回調
//
// 如果 返回 nil 則 在同個週期內不會被回調
//
// 如果 返回 非 nil 則 在同個週期內 會一直重複調用直到 週期結束或 函數 返回 nil
type CrontabCallback func(c *Crontab, t time.Time) error

// Crontab 定時任務
type Crontab struct {
	//分鐘 [0,59]
	Minute int

	//小時 [0,23]
	Hour int

	//號數 [1,31]
	Day int

	//月份 [1,12]
	Month int

	//星期 [0,7] 0 和 7 都是星期天
	Week int

	//結束標記
	chExit chan bool
	//wait 標記
	chWait chan bool
}

// NewCrontab 創建一個 Crontab 定時器
func NewCrontab(minute, hour, day, month, week int) (*Crontab, error) {
	cb := Crontab{Minute: minute,
		Hour:  hour,
		Day:   day,
		Month: month,
		Week:  week,
	}
	e := cb.IsOk()
	if e != nil {
		return nil, e
	}
	return &cb, nil
}

// String 返回 對應的 配置 字符串
func (c *Crontab) String() (string, error) {
	e := c.IsOk()
	if e != nil {
		return "", e
	}

	var buf bytes.Buffer
	arrs := []int{c.Minute,
		c.Hour,
		c.Day,
		c.Month,
		c.Week,
	}
	for i, v := range arrs {
		if i != 0 {
			buf.WriteString("	")
		}
		if v == CrontabAny {
			buf.WriteString("*")
		} else {
			buf.WriteString(fmt.Sprint(v))
		}
	}
	return buf.String(), nil
}

// FromString 由配置 字符串 初始化
func (c *Crontab) FromString(str string) error {
	str = strings.TrimSpace(str)
	reg, e := regexp.Compile(`([0-9]{1,2})|(\*)`)
	if e != nil {
		return e
	}
	strs := reg.FindAllString(str, -1)
	if len(strs) < 5 {
		return errors.New("need 5 item for Minute,Hour,Date,Month,Week")
	}

	var minute, hour, day, month, week int
	//minute
	if strs[0] == CrontabAnyName {
		minute = CrontabAny
	} else {
		v, e := strconv.ParseInt(strs[0], 10, 32)
		if e != nil || v < 0 || v > 59 {
			return errors.New(CrontabEmsgMinute)
		}
		minute = int(v)
	}

	//hour
	if strs[1] == CrontabAnyName {
		hour = CrontabAny
	} else {
		v, e := strconv.ParseInt(strs[1], 10, 32)
		if e != nil || v < 0 || v > 23 {
			return errors.New(CrontabEmsgHour)
		}
		hour = int(v)
	}

	//day
	if strs[2] == CrontabAnyName {
		day = CrontabAny
	} else {
		v, e := strconv.ParseInt(strs[2], 10, 32)
		if e != nil || v < 1 || v > 31 {
			return errors.New(CrontabEmsgDay)
		}
		day = int(v)
	}
	//month
	if strs[3] == CrontabAnyName {
		month = CrontabAny
	} else {
		v, e := strconv.ParseInt(strs[3], 10, 32)
		if e != nil || v < 1 || v > 12 {
			return errors.New(CrontabEmsgMonth)
		}
		month = int(v)
	}
	//week
	if strs[4] == CrontabAnyName {
		week = CrontabAny
	} else {
		v, e := strconv.ParseInt(strs[4], 10, 32)
		if e != nil || v < 0 || v > 7 {
			return errors.New(CrontabEmsgWeek)
		}
		week = int(v)
	}

	c.Minute = minute
	c.Hour = hour
	c.Day = day
	c.Month = month
	c.Week = week
	return nil
}

// IsOk 返回 配置 是否合法
func (c *Crontab) IsOk() error {
	if c.Minute != CrontabAny &&
		(c.Minute < 0 || c.Minute > 59) {
		return errors.New(CrontabEmsgMinute)
	}
	if c.Hour != CrontabAny &&
		(c.Hour < 0 || c.Hour > 23) {
		return errors.New(CrontabEmsgHour)
	}
	if c.Day != CrontabAny &&
		(c.Day < 1 || c.Day > 31) {
		return errors.New(CrontabEmsgDay)
	}
	if c.Month != CrontabAny &&
		(c.Month < 1 || c.Month > 12) {
		return errors.New(CrontabEmsgMonth)
	}
	if c.Week != CrontabAny &&
		(c.Week < 0 || c.Week > 7) {
		return errors.New(CrontabEmsgWeek)
	}
	return nil
}

// Run 運行 定時器
func (c *Crontab) Run(callback CrontabCallback) {
	chWait := make(chan bool)
	chExit := make(chan bool)
	c.chWait = chWait
	c.chExit = chExit
	var last time.Time
	var ec error
	go func() {
		for {
			// 計算 下個 運行 時間
			now := time.Now()
			if last.IsZero() {
				last = c.nextTime(now)
			} else {
				begin := now
				if ec == nil {
					if c.Month != CrontabAny {
						// 一月內 不要重複執行
						currentYear, currentMonth, _ := now.Date()
						currentLocation := now.Location()

						firstOfMonth := time.Date(currentYear, currentMonth, 1, 0, 0, 0, 0, currentLocation)
						lastOfMonth := firstOfMonth.AddDate(0, 1, -1)

						begin = lastOfMonth.Add(Day)
					} else if c.Week != CrontabAny {
						// 一周時內 不要重複執行
						if now.Weekday() == last.Weekday() &&
							now.Month() == last.Month() &&
							now.Year() == last.Year() {
							begin = now.Add(Day*(7-(time.Duration)(now.Weekday())) - time.Hour*(time.Duration)(now.Hour()) - time.Minute*(time.Duration)(now.Minute()))
						}
					} else if c.Day != CrontabAny {
						// 一天時內 不要重複執行
						if now.Day() == last.Day() &&
							now.Month() == last.Month() &&
							now.Year() == last.Year() {
							begin = now.Add(Day - time.Hour*(time.Duration)(now.Hour()) - time.Minute*(time.Duration)(now.Minute()))
						}
					} else if c.Hour != CrontabAny {
						// 一小時內 不要重複執行
						if now.Hour() == last.Hour() &&
							now.Day() == last.Day() &&
							now.Month() == last.Month() &&
							now.Year() == last.Year() {
							begin = now.Add(time.Hour - time.Minute*(time.Duration)(now.Minute()))
						}
					} else {
						// 一分種內 不要重複執行
						if now.Minute() == last.Minute() &&
							now.Hour() == last.Hour() &&
							now.Day() == last.Day() &&
							now.Month() == last.Month() &&
							now.Year() == last.Year() {
							begin = now.Add(time.Minute)
						}
					}

				}
				last = c.nextTime(begin)

			}
			d := last.Sub(now)
			t := time.NewTimer(d)
			select {
			case t := <-t.C:
				// 到時間 執行 定時任務
				ec = callback(c, t)
			case <-chExit:
				// 定時器 已經關閉 退出

				if !t.Stop() {
					<-t.C
				}
				close(c.chWait)
				return
			}
		}
	}()
}

// 計算 下個任務 的 執行時間
func (c *Crontab) nextTime(t time.Time) (next time.Time) {
	if c.isShouldRun(t, 0) {
		next = t
		return
	}
	// 分鐘
	t = c.nextMinute(t)
	if c.isShouldRun(t, 1) {
		next = t
		return
	}

	// 小時
	t = c.nextHour(t)
	for !c.isShouldRun(t, 2) {
		t = t.Add(time.Hour * 24)
	}
	next = t
	return
}

// nextHour 設置 下個 任務 執行 小時
func (c *Crontab) nextHour(t time.Time) time.Time {
	if c.Hour == CrontabAny {
		next := Day - time.Duration(t.Hour())*time.Hour
		t = t.Add(next)
		return t
	}
	v := t.Hour()
	if v <= c.Hour {
		next := time.Duration(c.Hour-v) * time.Hour
		t = t.Add(next)
	} else {
		next := Day - time.Duration(t.Hour()-c.Hour)*time.Hour
		t = t.Add(next)
	}
	return t
}

// 當前無法執行 nextMinute 設置 下個 任務 執行 分鐘
func (c *Crontab) nextMinute(t time.Time) time.Time {
	// 當前小時 無法執行 進入下個 小時
	if c.Minute == CrontabAny {
		next := time.Hour - time.Duration(t.Minute())*time.Minute
		t = t.Add(next)
		return t
	}

	v := t.Minute()
	if v <= c.Minute {
		next := time.Duration(c.Minute-v) * time.Minute
		t = t.Add(next)
	} else {
		next := time.Hour - time.Duration(t.Minute()-c.Minute)*time.Minute
		t = t.Add(next)
	}
	return t
}

// Close 停止 定時器
func (c *Crontab) Close() {
	ch := c.chExit
	if ch != nil {
		c.chExit = nil
		close(ch)
	}
}

// Wait 等待 定時器 結束
func (c *Crontab) Wait() {
	ch := c.chWait
	if ch != nil {
		<-ch
	}
}

// isShouldRun 返回是否需要 運行
//
// skip 要 跳過的 驗證
func (c *Crontab) isShouldRun(t time.Time, skip int) bool {
	if skip < 1 {
		minute := t.Minute()
		if c.Minute != CrontabAny && c.Minute != minute {
			return false
		}
	}

	if skip < 2 {
		hour := t.Hour()
		if c.Hour != CrontabAny && c.Hour != hour {
			return false
		}
	}

	if skip < 3 {
		day := t.Day()
		if c.Day != CrontabAny && c.Day != day {
			return false
		}
	}

	if skip < 4 {
		month := int(t.Month())
		if c.Month != CrontabAny && c.Month != month {
			return false
		}
	}

	if skip < 5 {
		week := int(t.Weekday())
		if c.Week != CrontabAny {
			if c.Week == 7 && week != 0 {
				return false
			}
			if c.Week != week {
				return false
			}
		}
	}
	return true
}

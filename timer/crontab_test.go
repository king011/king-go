package timer

import (
	"testing"
	"time"
)

func TestCrontabIsOk(t *testing.T) {
	cb := Crontab{Minute: CrontabAny,
		Hour:  CrontabAny,
		Day:   CrontabAny,
		Month: CrontabAny,
		Week:  CrontabAny,
	}
	e := cb.IsOk()
	if e != nil {
		t.Fatal(e)
	}

	//Minute
	cb.Minute = -2
	e = cb.IsOk()
	if e.Error() != CrontabEmsgMinute {
		t.Fatal("Minute small than 0 not work")
	}
	cb.Minute = 60
	e = cb.IsOk()
	if e.Error() != CrontabEmsgMinute {
		t.Fatal("Minute large than 59 not work")
	}
	cb.Minute = CrontabAny

	//Hour
	cb.Hour = -2
	e = cb.IsOk()
	if e.Error() != CrontabEmsgHour {
		t.Fatal("Hour small than 0 not work")
	}
	cb.Hour = 24
	e = cb.IsOk()
	if e.Error() != CrontabEmsgHour {
		t.Fatal("Hour large than 23 not work")
	}
	cb.Hour = CrontabAny

	//Day
	cb.Day = 0
	e = cb.IsOk()
	if e.Error() != CrontabEmsgDay {
		t.Fatal("Day small than 1 not work")
	}
	cb.Day = 32
	e = cb.IsOk()
	if e.Error() != CrontabEmsgDay {
		t.Fatal("Day large than 31 not work")
	}
	cb.Day = CrontabAny

	//Month
	cb.Month = 0
	e = cb.IsOk()
	if e.Error() != CrontabEmsgMonth {
		t.Fatal("Month small than 1 not work")
	}
	cb.Month = 13
	e = cb.IsOk()
	if e.Error() != CrontabEmsgMonth {
		t.Fatal("Month large than 12 not work")
	}
	cb.Month = CrontabAny

	//Week
	cb.Week = -2
	e = cb.IsOk()
	if e.Error() != CrontabEmsgWeek {
		t.Fatal("Week small than 0 not work")
	}
	cb.Week = 8
	e = cb.IsOk()
	if e.Error() != CrontabEmsgWeek {
		t.Fatal("Week large than 7 not work")
	}
	cb.Week = CrontabAny

	//min
	cb.Minute = 0
	cb.Hour = 0
	cb.Day = 1
	cb.Month = 1
	cb.Week = 0
	e = cb.IsOk()
	if e != nil {
		t.Fatal(e)
	}

	//max
	cb.Minute = 59
	cb.Hour = 23
	cb.Day = 31
	cb.Month = 12
	cb.Week = 7
	e = cb.IsOk()
	if e != nil {
		t.Fatal(e)
	}

}
func TestCrontabString(t *testing.T) {
	cb := Crontab{Minute: 59,
		Hour:  23,
		Day:   31,
		Month: CrontabAny,
		Week:  7,
	}

	//String
	cb.Month = CrontabAny
	str, _ := cb.String()
	if str != "59	23	31	*	7" {
		t.Fatal("String() not work")
	}

	var cb1 Crontab
	e := cb1.FromString(str)
	if e != nil {
		t.Fatal(e)
	}

	str1, _ := cb1.String()
	if str1 != str {
		t.Fatal("FromString not work")
	}
}
func TestCrontabShouldRun(t *testing.T) {

	cb := Crontab{Minute: CrontabAny,
		Hour:  CrontabAny,
		Day:   CrontabAny,
		Month: CrontabAny,
		Week:  CrontabAny,
	}
	now := time.Now()
	if !cb.isShouldRun(now, 0) {
		t.Fatal("isShouldRun not work CrontabAny")
	}

	cb.Minute = now.Minute()
	cb.Hour = now.Hour()
	cb.Day = now.Day()
	cb.Month = int(now.Month())
	cb.Week = int(now.Weekday())
	if !cb.isShouldRun(now, 0) {
		t.Fatal("isShouldRun not work")
	}

	now = time.Now()
	cb.Minute = now.Minute()
	cb.Hour = now.Hour() - 1
	cb.Day = now.Day()
	cb.Month = int(now.Month())
	cb.Week = int(now.Weekday())
	if cb.isShouldRun(now, 0) {
		t.Fatal("isShouldRun not work")
	}
}
func TestCrontab(t *testing.T) {

	cb, e := NewCrontab(CrontabAny, CrontabAny, CrontabAny, CrontabAny, CrontabAny)
	if e != nil {
		t.Fatal(e)
	}

	v := 0
	cb.Run(func(c *Crontab, t time.Time) (e error) {
		v++
		c.Close()
		return
	})
	cb.Wait()
	if v != 1 {
		t.Fatal("error", v)
	}
}
func TestNextTime(t *testing.T) {
	/**/
	{
		// Minute
		layout := "2006-01-02 15:04"
		now, _ := time.Parse(layout, "2006-01-02 15:04")
		last := now.Add(time.Minute)
		cb, e := NewCrontab(now.Minute(), CrontabAny, CrontabAny, CrontabAny, CrontabAny)
		if e != nil {
			t.Fatal(e)
		}
		str := now.Add(time.Hour).Format(layout)
		val := cb.nextTime(last).Format(layout)
		if str != val {
			t.Fatalf("test minute want [%s] but get [%s]", str, val)
		}
	}
	/**/
	/**/
	{
		// Minute
		layout := "2006-01-02 15:04"
		now, _ := time.Parse(layout, "2006-01-02 15:04")
		last := now.Add(time.Minute)
		cb, e := NewCrontab(10, CrontabAny, CrontabAny, CrontabAny, CrontabAny)
		if e != nil {
			t.Fatal(e)
		}
		str := now.Add(time.Minute * 6).Format(layout)
		val := cb.nextTime(last).Format(layout)
		if str != val {
			t.Fatalf("test minute want [%s] but get [%s]", str, val)
		}
	}
	/**/

	/**/
	{
		// Hour
		layout := "2006-01-02 15:04"
		now, _ := time.Parse(layout, "2006-01-02 15:04")
		last := now.Add(time.Hour)
		cb, e := NewCrontab(CrontabAny, now.Hour(), CrontabAny, CrontabAny, CrontabAny)
		if e != nil {
			t.Fatal(e)
		}
		str := now.Add(time.Hour*24 - time.Minute*(time.Duration)(now.Minute())).Format(layout)
		val := cb.nextTime(last).Format(layout)
		if str != val {
			t.Fatalf("test minute want [%s] but get [%s]", str, val)
		}
	}
	/**/
	/**/
	{
		// Hour
		layout := "2006-01-02 15:04"
		now, _ := time.Parse(layout, "2006-01-02 15:04")
		last := now.Add(time.Hour)
		cb, e := NewCrontab(CrontabAny, 17, CrontabAny, CrontabAny, CrontabAny)
		if e != nil {
			t.Fatal(e)
		}
		str := now.Add(time.Hour*2 - time.Minute*(time.Duration)(last.Minute())).Format(layout)
		val := cb.nextTime(last).Format(layout)
		if str != val {
			t.Fatalf("test minute want [%s] but get [%s]", str, val)
		}
	}
	/**/

	{
		// Day
		layout := "2006-01-02 15:04"
		now, _ := time.Parse(layout, "2006-01-02 15:04")
		last := now.Add(time.Hour)
		cb, e := NewCrontab(CrontabAny, CrontabAny, 5, CrontabAny, CrontabAny)
		if e != nil {
			t.Fatal(e)
		}
		str := now.Add(time.Hour*24*3 - time.Hour*(time.Duration)(now.Hour()) - time.Minute*(time.Duration)(now.Minute())).Format(layout)
		val := cb.nextTime(last).Format(layout)
		if str != val {
			t.Fatalf("test minute want [%s] but get [%s]", str, val)
		}
	}
}

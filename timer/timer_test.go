package timer_test

import (
	"gitlab.com/king011/king-go/timer"
	"testing"
)

func TestTimer(t *testing.T) {
	str := "2Week 6Day 23Hour 59Minute 59Second 999Millisecond 999Microsecond"
	duration := timer.Week*2 +
		6*timer.Day +
		23*timer.Hour +
		59*timer.Minute +
		59*timer.Second +
		999*timer.Millisecond +
		999*timer.Microsecond
	if str != timer.ToString(duration) {
		t.Fatal("ToString not work")
	}

	duration1, e := timer.ToDuration(str)
	if e != nil {
		t.Fatal(e)
	}
	if duration1 != duration {
		t.Fatal("ToDuration not work")
	}

	duration1, e = timer.ToDuration("1Week6Day1Week23Hour59Minute59Second999Millisecond999Microsecond")
	if e != nil {
		t.Fatal(e)
	}
	if duration1 != duration {

		t.Fatal("ToDuration not work")
	}

	duration1, e = timer.ToDuration("1Week	6Day 1Week 23Hour 59Minute 59Second 999Millisecond 999Microsecond")
	if e != nil {
		t.Fatal(e)
	}
	if duration1 != duration {
		t.Fatal("ToDuration not work")
	}

	duration1, e = timer.ToDuration("1Wek6Day1Week23Hour59Minute59Second999Millisecond999Microsecond")
	if e == nil {
		t.Fatal("ToDuration not work")
	}
}

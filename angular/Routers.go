package angular

import (
	"os"
	"strings"
)

// Routers 包含 多種語言的 angular 項目
type Routers struct {
	// url 根目錄
	rootURL string
	// angular 檔案夾
	rootPath string

	routers map[string]IRouter
}

// NewRouters .
func NewRouters(rootURL, rootPath string) *Routers {
	return &Routers{
		rootURL:  rootURL,
		rootPath: rootPath,
		routers:  make(map[string]IRouter),
	}
}

// Add 增加一個支持的 語言
func (r *Routers) Add(locale string) {
	r.routers[locale] = New(
		r.rootURL+"/"+locale+"/",
		r.rootPath+"/"+locale+"/",
	)
}

// Filepath 返回 檔案路徑
// 靜態檔案直接返回 其它全部以 index.html 返回
func (r *Routers) Filepath(requestURL string) (dist string, e error) {
	root := r.rootURL
	root += "/"

	path := requestURL
	if strings.HasPrefix(path, root) {
		path = path[len(root):]
	}
	strs := strings.SplitN(path, "/", 2)
	if len(strs) == 0 {
		e = os.ErrNotExist
		return
	}
	locale := strs[0]

	a := r.routers[locale]
	if a == nil {
		e = os.ErrNotExist
		return
	}

	dist, e = a.Filepath(requestURL)
	return
}

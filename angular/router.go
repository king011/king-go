package angular

import (
	"os"
	"sync"
)

const angularIndex = "index.html"

// _Angular 一個 Angular 項目
type _Angular struct {
	// 讀寫鎖
	sync.RWMutex
	rootURL       string
	rootDirectory string

	// 緩存 存在的 檔案
	Keys map[string]bool
}

// Filepath 返回 請求 對應的 檔案路徑
func (a *_Angular) Filepath(filename string) (filepath string, e error) {
	// 去掉 url 頭部
	rootURL := a.rootURL
	if len(filename) <= len(rootURL) {
		filename = angularIndex
	} else {
		filename = filename[len(rootURL):]
	}

	// 查找 檔案
	rootDirectory := a.rootDirectory
	if filename == angularIndex {
		filepath = rootDirectory + filename
	} else {
		a.RLock()
		ok := a.Keys[filename]
		a.RUnlock()
		if ok {
			filepath = rootDirectory + filename
		} else {
			filepath = rootDirectory + filename
			_, e = os.Stat(filepath)
			if e == nil {
				a.Lock()
				a.Keys[filename] = true
				a.Unlock()
			} else {
				if os.IsNotExist(e) {
					e = nil
					filepath = rootDirectory + angularIndex
				} else {
					return
				}
			}
		}
		return
	}
	return
}

package angular

// IRouter angular 路由
type IRouter interface {
	// 返回 檔案路徑
	// 靜態檔案直接返回 其它全部以 index.html 返回
	Filepath(src string) (dist string, e error)
}

// New 創建一個 路由
// rootURL angular root 路由
// rootDirectory angular 所在目錄
//
// New("/angular/zh-Hant/","/www/myweb/angular/zh-Hant/")
func New(rootURL, rootDirectory string) IRouter {
	return &_Angular{
		rootURL:       rootURL,
		rootDirectory: rootDirectory,
		Keys:          make(map[string]bool),
	}
}
